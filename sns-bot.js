const { RTMClient } = require('@slack/client');
const { arrangeText } = require('./utils/botUtils');
const { default: axios } = require('axios');
const { addArtistToGroup, botInsertArtist } = require('./service/artistService');
const token = 'xoxb-15296271571-3064137831462-kNa6IU0OnFPvMv5dJIoAzQTT';
const rtm = new RTMClient(token);

let groupId = '';
let kr = '';
let en = '';
let sns_enum = '';
let sns_id = '';


rtm.start();
rtm.on('message', async(event) => {
    const text = event.text;
    console.log(`${new Date} - ${text}\n\n`);
    if(text.includes(':') && text.includes('그룹아이디') && text.includes('\n')) {
        const artist = arrangeText(text);
        rtm.sendMessage('잠시만여..😓', event.channel);
        await botInsertArtist({ group_name: artist['그룹아이디'], kr_name: artist['한글이름'], en_name: artist['영어이름'], username: artist.sns_id.replace('https://www.instagram.com/', ''), sns_enum: artist.sns });
        rtm.sendMessage('추가됐습니다😀', event.channel);
    }
    if(text === 'help') {
        rtm.sendMessage(`아티스트 추가를 위해서는\n그룹아이디, 한글 이름, 영어 이름, instagram/twitter, sns_id를 입력해주세요\nex)\n그룹아이디:NCT\n한글이름:엔씨티\n영어이름:NCT\nsns:instagram\nsns_id:nct`, event.channel);
    }
})

