exports.isLoggedIn = (req, res, next) => {
  if (req.isAuthenticated() || process.env.NODE_ENV === 'development') {
    return next();
  }
  res.status(403).end();
  // Forbidden
};