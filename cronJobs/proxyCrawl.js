const { launchBrowser } = require("../utils/crawlUtils");
const puppeteer = require('puppeteer');

exports.getProxyList = async() => {
    try{
        let browser = await puppeteer.launch({
            headless : process.env.NODE_ENV === 'production',
            args : ['--ignore-certificate-errors']
        })
        let page = await browser.newPage();
        await page.goto('https://spys.one/free-proxy-list/KR/');
        const proxies = await page.evaluate(() => {
            const ips = Array.from(document.querySelectorAll('tr > td:first-of-type > .spy14')).map((v) => v.textContent.replace(/document\.write\(.+\)/, ''));
            const types = Array.from(document.querySelectorAll('tr > td:nth-of-type(2)')).slice(5).map((v) => v.textContent);
            const latencies = Array.from(document.querySelectorAll('tr > td:nth-of-type(6) .spy1')).map((v) => v.textContent);

            return ips.map((v, i) => {
                return {
                    ip : v,
                    type : types[i],
                    latency : latencies[i]
                };
            })
        });

        const fastest = proxies.filter((v) => v.type.startsWith('HTTPS'))
                .sort((a, b) => a.latency - b.latency); 
        await page.close();
        await browser.close();

        return fastest;
    }catch(err){
        console.error(err);
        throw err;
    }
}