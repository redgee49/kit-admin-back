await page.evaluate(() => {
    const temp = document.querySelectorAll('span[class*="__fb-dark-mode"]');
    Array.from(temp).forEach(el => el.parentNode.removeChild(el));
    document.querySelectorAll('video').forEach(video => video.pause());
  });

  try {
    await page.waitForSelector(`div[role="feed"]:nth-child(2) > div:nth-child(${j}) div[role="article"] b:not([style="display: none;"])`, {
      timeout: 3000,
    });

    await delay(500);
    await page.evaluate(
      `document.querySelector('div[role="feed"]:nth-child(2) > div:nth-child(${j}) div[role="article"]').scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"})`,
    );
    await page.hover(`div[role="feed"]:nth-child(2) > div:nth-child(${j}) div[role="article"] b:not([style="display: none;"])`);
    await page.waitForSelector('span[class*="__fb-dark-mode"]');
    date = await page.evaluate(() => {
      const temp = document.querySelectorAll('span[class*="__fb-dark-mode"]');
      const last = temp[temp.length - 1].textContent;
      console.log(last);
      return last;
    });

    if (moment(date, 'YYYY년 M월 DD일 dddd a H:mm').isSameOrBefore(lastRow.posted_date) || date.includes('2019')) {
      flag = false;
      break;
    }

    await page.evaluate(
      // eslint-disable-next-line
      ({ j }) => {
        const el = document.querySelector(`div div[role="main"] div div div[aria-posinset="${j}"]`);

        el.querySelector('div[data-ad-preview="message"] div[role="button"]') &&
          el.querySelector('div[data-ad-preview="message"] div[role="button"]').click(); // 더보기 버튼
      },
      { j },
    );

    const temp = await page.evaluate(
      // eslint-disable-next-line
      ({ j, date }) => {
        const el = document.querySelector(`div[role="feed"]:nth-child(2) > div:nth-child(${j}) div[role="article"]`);

        el.querySelector('div[data-ad-preview="message"] div[role="button"]') &&
          el.querySelector('div[data-ad-preview="message"] div[role="button"]').click(); // 더보기 버튼

        Array.from(el.querySelectorAll('div[data-ad-preview="message"] span > img')).forEach(element => {
          // eslint-disable-next-line
          element.textContent = v.getAttribute('alt');
          // emoji 텍스트로 변환
        });

        return {
          text: el.querySelector('div[data-ad-preview="message"] span') && el.querySelector('div[data-ad-preview="message"] span').textContent,
          date,
          temp_url:
            (el.querySelector('a[href*="/videos/"]') && el.querySelector('a[href*="/videos/"]').href) ||
            el.querySelector('a[href*="/posts/"]').href,
          src_array: [
            ...Array.from(el.querySelectorAll('div[style*="width: calc"] img')).map(v => v.src),
            ...Array.from(el.querySelectorAll('video')).map(v => v.src),
          ],
        };
      },
      { j, date },
    );

    console.log(date);
    posts.push(temp);