const pool = require('../config/muzSQL');

(async() => {
    const conn = await pool.getConnection();
    const [result] = await conn.query(`select * from SNS_contents_info group by user_id, href`);
    
    for(const [index, row] of Object.entries(result)){
        try{
            const [equalPosts] = await conn.query(`SELECT * FROM SNS_contents_info WHERE href = ? AND user_id = ?`, [row.href, row.user_id]);
            if(equalPosts.length > 1){
                const origin = equalPosts.pop();

                equalPosts.map(async(row) => {
                    await conn.query(`DELETE FROM SNS_contents_info WHERE content_id = ? AND board_id = ? AND user_id = ?`, [row.content_id, row.board_id, row.user_id]);
                    await conn.query(`DELETE FROM SNS_media_info WHERE content_id = ? AND artist_seq = ?`, [row.content_id, row.user_id]);
                })
            }

        }catch(err){
            console.log(err);
        }
    }
    process.exit(0);
})();