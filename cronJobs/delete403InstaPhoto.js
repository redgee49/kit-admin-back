const pool = require('../config/muzSQL');
const { fileSave } = require('../utils');
const axios = require('axios').default;

(async() => {
    const conn = await pool.getConnection();
    const [result] = await conn.query(`SELECT * FROM SNS_media_info where src like '%cdninstagram%'`);
    
    for(const [index, row] of Object.entries(result)){
        const {src} = row;
        try{

            await fileSave({ url : src, exec = 'jpg' });

        }catch(err){
            if(err.response.status === 403){
                await conn.query(`DELETE FROM SNS_media_info WHERE src = ${src}`);
                console.log(src);
            }
        }finally{
            console.log((index / result.length) * 100);
        }
    }
    process.exit(0);
})();