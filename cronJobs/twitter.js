const moment = require('moment');
require('moment-timezone');

moment.tz.setDefault('Asia/Seoul');
const Twitter = require('twitter');
const path = require('path');

require('dotenv').config({ path: path.join(__dirname, '../.env') });

const client = new Twitter({
  consumer_key: process.env.TWIT_CONSUMER_KEY,
  consumer_secret: process.env.TWIT_CONSUMER_SECRET,
  access_token_key: process.env.ACCESS_TOKEN_KEY,
  access_token_secret: process.env.ACCESS_TOKEN_SECRET,
});

const fetchTimeLine = params => {
  return new Promise((res, rej) => {
    client.get('statuses/user_timeline', params, (err, result) => {
      if (err) {
        rej(err[0]);
      }

      res(result);
    });
  });
};

const fetchProfileImage = params => {
  return new Promise((res, rej) => {
    client.get('users/show', params, (err, result) => {
      if (err || err?.length || !result?.profile_image_url) {
        rej(err[0]);
      }

      res(result.profile_image_url.replace('_normal', ''));
    });
  });
};

const crawl = async ({ user_id, max_id, since_id, idate }) => {
  const params = {
    screen_name: user_id,
    count: 100,
    trim_user: true,
    exclude_replies: true,
    include_entities: false,
    include_rts: false,
    tweet_mode: 'extended',
  };

  if (max_id) params.max_id = max_id;
  if (since_id) params.since_id = Number(since_id);

  try {
    let result = await fetchTimeLine(params);

    result = result.map(v => {
      const thumbnail = v.extended_entities
        ? v.extended_entities.media.map(media => {
          if (media.type === 'video') {
            return media.media_url_https;
          }
          return null;
        })
        : [null];

      return {
        description: v.full_text.replace(/&lt;/g, '<').replace(/&gt;/g, '>'),
        href: v.id_str,
        idate: v.created_at && moment(v.created_at.replace(' +0000', ''), 'ddd MMM DD HH:mm:ss YYYY').add(9, 'hours').format('YYYY-MM-DD HH:mm:ss'),
        mediaList:
          v.extended_entities ?
          v.extended_entities.media.map(media => {
            if (media.type === 'video') {
              return media.video_info.variants.find(el => el.content_type.includes('mp4')).url;
            }
            return media.media_url_https;
          }) : [],
        thumbnail,
      };
    });

    const last_date = result.length && result[result.length - 1].idate;

    if (!last_date || result.length === 1) {
      return result.filter(v => moment(v.idate, 'YYYY-MM-DD HH:mm:ss').isSameOrAfter(idate) && v.href !== since_id);
    }
    if (result.every(v => moment(v.idate, 'YYYY-MM-DD HH:mm:ss').isAfter(idate))) {
      // 다음페이지로 재귀
      const nextPage = await crawl({ user_id, max_id: result[result.length - 1].href, idate });
      return [...result, ...nextPage];
    }

    return result.filter(v => moment(v.idate, 'YYYY-MM-DD HH:mm:ss').isSameOrAfter(idate) && v.href !== since_id);
  } catch (e) {
    console.error(e);
  }
};

module.exports = {
  crawl,
  fetchProfileImage
};
