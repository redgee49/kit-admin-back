const moment = require('moment');
require('moment-timezone');

moment.tz.setDefault('Asia/Seoul');
const path = require('path');
const axios = require('axios').default;

require('dotenv').config({ path: path.join(__dirname, '../.env') });

async function getChannel({ channelId }) {
  /**
   *   @description return input channel's upload path id
   *   @param  {string} channelId : 채널 아이디
   *   @return {string} channel's upload id for playlistId
   */
  try {
    const response = await axios.get(
      'https://www.googleapis.com/youtube/v3/channels',{
        params: {
          part: 'id,snippet,contentDetails',
          id: channelId,
          key: 'AIzaSyDdK9yif8v4eFVEu0V0SV19OHcrxfLwDLo'
        }
      }
    );
    return {
      name: response.data.items[0].snippet.title,
      profilePic : response.data.items[0].snippet.thumbnails.medium || response.data.items[0].snippet.thumbnails.high || response.data.items[0].snippet.thumbnails.default,
      uploadListId: response.data.items[0].contentDetails?.relatedPlaylists?.uploads
    };
  } catch (e) {
    console.log(e);
  }
}

async function getPlaylistItems({ playlistId, maxResults = 50, nextPageToken = null }) {
  /**
   *   @description return input channel's recent upload videos
   *   @param  {string} playlistId : channel's recent playlistId
   *   @param  {number} maxResults : result max count (default 5)
   *   @return {string} channel's recent videos
   */
  try {
    const response = await axios.get(
      'https://www.googleapis.com/youtube/v3/playlistItems?part=snippet',{
        params: {
          playlistId,
          maxResults,
          pageToken: nextPageToken ? nextPageToken : null,
          key: 'AIzaSyDdK9yif8v4eFVEu0V0SV19OHcrxfLwDLo'
        }
      }
    );
    return response.data;
  } catch (e) {
    console.log(e);
  }
}

function arrange({ contents = [] }){
  const result = contents.items?.map((item) => {
    return {
      href: item.snippet.resourceId.videoId,
      description: item.snippet.description,
      title: item.snippet.title,
      thumbnail: item.snippet.thumbnails.high || item.snippet.thumbnails.medium || item.snippet.thumbnails.default,
      idate: moment(item.snippet.publishedAt).format('YYYY-MM-DD HH:mm:ss')
    }
  })
  return result;
}

module.exports = {
  getChannel,
  getPlaylistItems,
  arrange,
};
