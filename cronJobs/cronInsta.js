const pool = require('../config/muzSQL');
const { fetchProfileImage, recursiveFetchPosts } = require('./instagram');
const { findAllArtist, findLastRow } = require('../query/CRAWL_QUERY');
const { updateProfileImage, insertPost } = require('../utils/insertSNS');
const Instagram = require('instagram-web-api');
const slack = require('../utils/instaSlack');
const { log } = require('../utils');

(async () => {
  const conn = await pool.getConnection();
  const finished = [];
  const results = [];
  try{
    const client = new Instagram({ username : 'h0ng.jh', password : 'ghdgns1256!' });
    await client.login();
    const [artistList] = await conn.query(findAllArtist, ['instagram']);
    for(const artist of artistList) {
      const [lastPost] = await conn.query(findLastRow, [artist.seq, 'instagram']);
      const profileImage = await fetchProfileImage({ client, sns_id : artist.sns_id });
      if(profileImage) {
        await updateProfileImage({ seq: artist.seq, url: profileImage ,sns_enum: 'instagram' });
      }
      /* results 배열에 이미 같은 계정에서 수집한 데이터를 찾는다. */
      const exists = finished.find((el) => el.sns_id === artist.sns_id);
      let crawledData = (exists && exists.data) || await recursiveFetchPosts({
        client,
        username: artist.sns_id,
        lastRow: lastPost[0],
      })
      finished.push({ sns_id : artist.sns_id, data : crawledData });
      if(artist.filtering_search) {
        /* 그룹 계정의 데이터에서 아티스트 kr 이름이나 en 이름이 포함되어있는 경우로 필터링 */
        crawledData = crawledData.filter(item => item.description.includes(artist.artist_nick_kr) || item.description.includes(artist.artist_nick_en));
      }
      log(`${artist.sns_id} - ${crawledData?.length}건`);
      results.push(`${artist.artist_nick_kr} - ${crawledData.length}건`);
      for(const data of (crawledData || []).reverse()) {
        await insertPost({ post: data, artist });
      }
    }
  } catch (err) {
    if(err.error.message === 'checkpoint_required') {
      await slack.error({ title : '계정이 잠겼어요...😭', text: '휴대폰 인증이 필요합니다 ㅠㅠ' });
    }
    console.error(err.message);
  } finally {
    await slack.info({ title : '인스타 크롤링 완료', text: results.join('\n') });
  }

  process.exit(0);
})();
