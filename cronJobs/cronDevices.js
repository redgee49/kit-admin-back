const pool = require('../config/kihno');
const axios = require('axios');
const { launchBrowser } = require('../utils/crawlUtils');
const moment = require('moment');

(async() => {
    const conn = await pool.getConnection();
    const [browser, page] = await launchBrowser();
    await page.goto('https://storage.googleapis.com/play_public/supported_devices.html');
    await page.waitForSelector('table', 3000);
    
    const devices = await page.evaluate(() => {
        const brand_names = Array.from(document.querySelectorAll('td:nth-of-type(1)')).map((v) => v.textContent);
        const marketing_names = Array.from(document.querySelectorAll('td:nth-of-type(2)')).map((v) => v.textContent);
        const model_names = Array.from(document.querySelectorAll('td:nth-of-type(4)')).map((v) => v.textContent);

        if(brand_names.length !== marketing_names.length || marketing_names.length !== model_names.length){
            throw Error('Invalid data format (length)');
        }
      
        return Array(model_names.length).fill().map((v, i) => {
            return {
                brand_name : brand_names[i],
                marketing_name : marketing_names[i],
                model_name : model_names[i]
            }
        })
    });
    let count = 0;
    const filtered = devices.filter((v) => v.marketing_name);
    for(const row of filtered){
        try{
            await conn.query(`
                INSERT INTO device_model_name_info(brand_name, marketing_name, model_name)
                VALUES (?, ?, ?)
            `, [row.brand_name, row.marketing_name, row.model_name]);
            console.log(`${moment().format('YYYY-MM-DD')} - ${row.model_name} INSERTED`);
            count++;
        }catch(err){
            if(!err.message.includes('Duplicate entry')){
                console.error(err);
            }
            continue;
        }
    }
    console.log('FINISHED');
    browser.close();
    await axios.post(`https://hooks.slack.com/services/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`, {
        text : `추가된 기기 : ${count}개 크롤링 완료`
    })
    process.exit(0);
})();

// 넘어온 값이 빈값인지 체크합니다.
// !value 하면 생기는 논리적 오류를 제거하기 위해
// 명시적으로 value == 사용
// [], {} 도 빈값으로 처리
