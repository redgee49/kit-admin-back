const puppeteer = require('puppeteer-extra');
const moment = require('moment');
const path = require('path');
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
const { delay } = require('../utils');
const { insertPost } = require('../utils/insertSNS');
const { findLastRow } = require('../query/CRAWL_QUERY');

puppeteer.use(StealthPlugin());

let count = 0;

moment.locale('ko', {
  weekdays: ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'],
  meridiem: e => {
    if (e > 12) {
      return '오후';
    }
    return '오전';
  },
});

require('dotenv').config({ path: path.join(__dirname, '../.env') });
require('moment-timezone');

moment.tz.setDefault('Asia/Seoul');

const waitor = {
  waitUntil: 'networkidle0',
};

const multiCrawl = async({ artistList, conn }) => {
  const browser = await puppeteer.launch({
    headless: process.env.NODE_ENV === 'production',
    // headless : true,
    defaultViewport: null,
    args: ['--disable-notifications'],
  });

  const page = await browser.newPage();

  await page.setViewport({ width: 1920, height: 1680 });
  await page.setUserAgent(`Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3617.0 Safari/537.36`);
  await page.goto(`https://www.facebook.com`);

  await Promise.all([page.waitForSelector('[name="email"]'), page.waitForSelector('[name="pass"]')]);

  await page.type('[name="email"]', process.env.FACEBOOK_ID);
  await page.type('[name="pass"]', process.env.FACEBOOK_PW);

  await Promise.all([page.click('[type="submit"]'), page.waitForNavigation(waitor)]);
  console.log('로그인 완료');

  const finished = [];

  try{
    for(const artist of artistList){
      const [lastRow] = await conn.query(findLastRow, [artist.seq]);
      const exist = finished.find((el) => el.sns_id === artist.sns_id);
  
      const crawledData =  (exist && exist.data) || await crawl({ browser : browser, page, sns_id : artist.sns_id, lastRow : lastRow.length && lastRow[0] || { idate: moment().subtract(3, 'days').format('YYYY-MM-DD 00:00:00') } });
      for(const row of crawledData){
        await insertPost({ post : row, artist });
      }
  
      exist ? console.log('\x1b[36m%s\x1b[0m', `중복 아티스트: ${artist.artist_nick_kr} ${crawledData.length} 완료`)
      : console.log(`${moment().format('YYYY-MM-DD HH:mm:ss')} ${artist.artist_nick_kr} - ${crawledData.length} 완료`);
      finished.push({ sns_id : artist.sns_id, data : crawledData });
    }
  }catch(e){
    console.log(e);
  }finally{
    await browser.close();
  }
}


const crawl = async ({ browser, page, sns_id, lastRow }) => {
  await page.goto(`https://www.facebook.com/${sns_id}/posts/?ref=page_internal`)
  const posts = [];

  const loop = async() => {
    count += 1;
    await page.waitFor(2000);
    await page.evaluate(() => {
      scrollTo(0, 100); 
      scrollTo(0, 0);
    });
    // 브라우저 화면에 보이는 포스트를 active 상태로 만들기 위함

    await page.waitForSelector(`div[data-insertion-position]`);
    
    const dateVal = await page.evaluate(() => {
      return document.querySelector(`div[data-insertion-position] abbr`).getAttribute('data-utime');
    });

    const date = moment(new Date(dateVal * 1000)).format('YYYY-MM-DD HH:mm:ss');


    

    await page.evaluate(() => {
      document.querySelectorAll('.see_more_link_inner').forEach(v => {
        v.click(); // 더보기 버튼
        v.parentNode.removeChild(v.parentNode.childNodes[0]); // 더보기 텍스트 삭제
      });
    });

    const set = new Set();
    const imageContainer = await page.evaluate(() => {
      const singleArticle = document.querySelector(`div[data-insertion-position]`);
      return (singleArticle && [...Array.from(singleArticle.querySelectorAll('img[class*="scaled"][src*="scontent"]')).map(v => v.src)]) || []
    });

    if(imageContainer.length && moment(date, 'YYYY-MM-DD HH:mm:ss').isSameOrAfter(lastRow.idate)){

      const mediaPage = await browser.newPage();
      const initialSrc = imageContainer[0];
      
      const imageHref = await page.evaluate(({ initialSrc }) => { 
        return document.querySelector(`img[src*="${initialSrc}"]`).parentNode.parentNode.getAttribute('ajaxify');
      }, { initialSrc });

      const addedCount = await page.evaluate(({ initialSrc }) => { 
        return document.querySelector(`img[src*="${initialSrc}"]`).parentNode.parentNode.parentNode.textContent.replace(/[^0-9]/g, "") || null;
      }, { initialSrc }) || 0;

      await mediaPage.goto(`https://www.facebook.com/${imageHref}`)
      try{
        await mediaPage.waitForSelector(`div[class*="fullScreenAvailable"][class*="pagingReady"]`);
      }catch(e){
        console.log(e);
        await mediaPage.screenshot({path : `./mediaError_${e.message.slice(0, 20)}.png`});
      }

      for(let index = 0; index < addedCount + imageContainer.length; index++){
        index > 0 && await mediaPage.keyboard.press('ArrowRight');
        await mediaPage.waitForSelector('img.spotlight:not([src*="gif"])');
        const source = await mediaPage.evaluate(() => {
          return document.querySelector('img.spotlight:not([src*="gif"])') && document.querySelector('img.spotlight:not([src*="gif"])').src
        })
        set.add(source);
        console.log(` ㄴ [${index}번째 : ${set.size}] 수집된 미디어 파일 url  : ${source.slice(-15)}`);
        await mediaPage.waitFor(1000);
      } 
      console.log(`==================================================\n`);
      await mediaPage.close();
    }

    const mediaList = Array.from(set);

    const temp = await page.evaluate(
      // eslint-disable-next-line
      ({ date, mediaList }) => {
        const singleArticle = document.querySelector(`div[data-insertion-position]`);

        return {
          description:
            (singleArticle && singleArticle.querySelector('.text_exposed') && singleArticle.querySelector('.text_exposed').textContent) ||
            (singleArticle.querySelector('div [data-testid="post_message"]') &&
              singleArticle.querySelector('div [data-testid="post_message"]').textContent),
          idate: date,
          href:
            (singleArticle && singleArticle.querySelector('a[href*="/videos/"]') && singleArticle.querySelector('a[href*="/videos/"]').href) ||
            (singleArticle.querySelector('a[href*="/posts/"]') && singleArticle.querySelector('a[href*="/posts/"]').href),
          mediaList
        };
      },
      { date, mediaList },
    );

    if (posts.length > 1 && moment(date, 'YYYY-MM-DD HH:mm:ss').isSameOrBefore(lastRow.idate)) {
      return;
    } 
    posts.push(temp);

    try {
      await page.evaluate(
        `document.querySelectorAll('div[data-insertion-position]')[0].parentNode.parentNode.parentNode
          .removeChild(document.querySelectorAll('div[data-insertion-position]')[0].parentNode.parentNode.parentNode.childNodes[0])`,
      );
      await page.evaluate(() => {
        document.querySelector('.uiMorePager a') && document.querySelector('.uiMorePager a').click();
      });
      await loop();
    } catch (e) {
      console.log(e);
      await page.screenshot({path : `./crawlError_${e.message.slice(0, 20)}.png`})
    }
  }
  
  await loop();
  // 재귀로 반복되는 loop 함수
  const crawledData = posts
                        .map(post => { 
                          // eslint-disable-next-line
                          post.href = post.href && post.href.slice(post.href.indexOf('https://www.facebook.com') + 'https://www.facebook.com'.length);

                          post.description = post.description && post.description.replace('  · 원본 보기  · 이 번역 평가하기', '');

                          post.meta =
                            (post.description &&
                              post.description.match(/(https?:\/\/[^\s]+)/g) &&
                              post.description.match(/(https?:\/\/[^\s]+)/g).length &&
                              post.description.match(/(https?:\/\/[^\s]+)/g)[0]) ||
                            null;

                          return post;
                        })
                        .filter(v => moment(v.idate).isSameOrAfter(lastRow.idate) && v.href !== lastRow.href);

  return crawledData;
};


module.exports = {
  multiCrawl,
};
