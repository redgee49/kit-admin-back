const { getAndroid, getApple, getAllList, getInvalidRankList } = require("../service/deviceService");
const moment = require('moment');
const slack = require('../utils/micSlack');

(async() => {
    const startDate = moment().subtract('30', 'days').format('YYYY-MM-DD');
    const endDate = moment().format('YYYY-MM-DD');
    
    const invalidList = await getInvalidRankList({ startDate, endDate });
    const text = invalidList.map(v => `${v.model_name} - ${v.count} <https://www.google.com/search?q=${v.model_name}|:link:>`)
                            .join('\n\n');
    await slack.info({ title: '100개이상 들어온 디바이스 리스트', text });

    const [android] = await getAndroid({ startDate, endDate });
    const [apple] = await getApple({ startDate, endDate });
    const [overall] = await getAllList({ startDate, endDate });

    await slack.info({ title : `🍎 APPLE Coverage (${startDate} - ${endDate})`, text: `${apple.TOTAL_Y_PERCENT.slice(0, 4)}%` });
    await slack.warn({ title : `💚 Android Coverage (${startDate} - ${endDate})`, text: `${android.TOTAL_Y_PERCENT.slice(0, 4)}%` });
    await slack.warn({ title : `📱 Overall Coverage (${startDate} - ${endDate})`, text: `${overall.TOTAL_Y_PERCENT.slice(0, 4)}%` });
    
    process.exit(0);
})();