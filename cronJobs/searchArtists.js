const { launchBrowser } = require("../utils/crawlUtils");

const artists = [
    '아스트로',
'블랙핑크',
'포레스텔라',
'아이즈원',
'김성규',
'러블리즈',
'마마무',
'몬스타엑스',
'남우현',
'NCT127',
'NCT 드림',
'넬',
'레드벨벳',
'샤이니',
'슈퍼주니어',
'태민',
'태연',
'트레저',
'워너원',
'안예은',
'바비',
'드림캐쳐',
'엑소',
'백현',
'세훈&찬열',
'첸',
'프로미스나인',
'에프엑스',
'여자친구',
'소녀시대 Oh!GG',
'소녀시대',
'구구단',
'세미나',
'우주힙쟁이',
'인투잇',
'종현',
'강승윤',
'비오브유',
'김우석',
'크나큰',
'규현',
'이진혁',
'이달의 소녀',
'최강창민',
'송민호',
'모모랜드',
'문별',
'나띠',
'NCT',
'엔플라잉',
'뉴이스트 W',
'뉴이스트',
'로제',
'설하윤',
'세븐틴',
'SF9',
'키',
'신화',
'슈퍼주니어 D&E',
'수호',
'슬기로운 의사생활',
'동방신기',
'유노윤호',
'업텐션',
'베리베리',
'빅스',
'레오',
'빅스 LR',
'라비',
'웨이션브이',
'위키미키',
'우즈',
'X1',
'김준수',
'예성',
'윤지성',
'윤아',
'업폴',
'크래비티',
'한승우',
'리사',
'스눕독',
'투유드림',
'도한세',
'전소미',
'투유드림',
'나다',
'리버티',
'김재중',
'김재환',
'수영'
];

(async() => {
    const [browser, page] = await launchBrowser();
    for(const artist of artists) {
        await page.goto(`https://search.naver.com/search.naver?query=${artist}`);
        const isGroup = await page.evaluate(() => {
            const labels = document.querySelectorAll('dl.txt_3 div.info_group');
            return Array.from(labels).find(v => v.textContent.includes('멤버'));
        })

        if(isGroup) {
            const sns_href = await page.evaluate(() => {
                const anchors = document.querySelectorAll('dl.txt_3 div.info_group a');
                return Array.from(anchors).find(el => el.href.includes('twitter.com'))?.href;
            })
            console.log(`${artist} equal ${sns_href}`);
            const members_anchor = await page.evaluate(() => {
                const divs = document.querySelectorAll('dl.txt_3 div.info_group');
                const dl_array = Array.from(divs).find(v => v.textContent.includes('멤버'));
                return Array.from(dl_array.querySelectorAll('dd a')).map(v => v.href)
            })
            for(const anchor of members_anchor) {
                await page.goto(anchor);
                const sns_href = await page.evaluate(() => {
                    const anchors = document.querySelectorAll('dl.txt_3 div.info_group a');
                    return Array.from(anchors).find(el => el.href.includes('twitter.com'))?.href;
                })
                const memberName = await page.evaluate(() => {
                    return document.querySelector('input[name="query"]').value;
                })
                if(sns_href){
                    console.log(`${artist}(${memberName}) equal ${sns_href}`);
                }else{
                    console.log(`${artist}(${memberName}) equal 없네여...`)
                }
            }
        }else{
            const sns_href = await page.evaluate(() => {
                const anchors = document.querySelectorAll('dl.txt_3 div.info_group a');
                return Array.from(anchors).find(el => el.href.includes('twitter.com'))?.href;
            })
            if(sns_href){
                console.log(`${artist} equal ${sns_href}`);
            }else{
                console.log(`${artist} equal 없네여...`)
            }
        }
    }
    browser.close();
})();