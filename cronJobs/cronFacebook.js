const pool = require('../config/muzSQL');
const { multiCrawl } = require('./facebook');

const { findAllArtist } = require('../query/CRAWL_QUERY');

(async () => {
  const conn = await pool.getConnection();

  const [artistList] = await conn.query(findAllArtist, ['facebook']);

  await multiCrawl({ artistList, conn });

  process.exit(0);
})();