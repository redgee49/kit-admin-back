const moment = require('moment');
const path = require('path');
const slack = require('../utils/instaSlack');
const { delay } = require('../utils');

require('dotenv').config({ path: path.join(__dirname, '../.env') });
require('moment-timezone');

moment.tz.setDefault('Asia/Seoul');

function arrangePosts(timelines) {
  return timelines.edges.map(post => {
    const mediaList = post.node.edge_sidecar_to_children
      ? post.node.edge_sidecar_to_children.edges.map((media) => {
        return media.node.is_video ? media.node.video_url : media.node.display_url;
      })
      : [post.node.is_video ? post.node.video_url : post.node.display_url];
    const text = post.node.edge_media_to_caption.edges[0] ? post.node.edge_media_to_caption.edges[0].node.text : null;
    const thumbnail = post.node.edge_sidecar_to_children ? 
                        post.node.edge_sidecar_to_children.edges.map((media) => {
                          return media.node.is_video ? media.node.display_url : null;
                          // 비디오만 썸네일 추가
                        }).filter(v => v)
                        : [post.node.is_video ? post.node.display_url : null].filter(v => v);

    return {
      description: text,
      href: post.node.shortcode,
      idate: moment.unix(post.node.taken_at_timestamp).format('YYYY-MM-DD HH:mm:ss'),
      mediaList : mediaList.reverse(),
      thumbnail,
      meta : (text && text.match(/(https?:\/\/[^\s]+)/g) && text.match(/(https?:\/\/[^\s]+)/g).length && text.match(/(https?:\/\/[^\s]+)/g)[0]) || null,
    };
  });
}

async function recursiveFetchPosts({ client, username, endCursor = null, edges = [], lastRow }) {
  try{
    await delay();
    const response = await client.getPhotosByUsername({ username, first: 12, after: endCursor });
    const timelines = response.user.edge_owner_to_timeline_media;

    const posts = arrangePosts(timelines);
    const result = [...edges, ...posts];

    const lastDate = (lastRow && lastRow.idate) || '2020-12-31';
    const lastURL = (lastRow && lastRow.href) || 'must not equal this string!';

    if (timelines.page_info.has_next_page && moment(posts?.[posts?.length - 1]?.idate, 'YYYY-MM-DD HH:mm:ss').isAfter(lastDate)) {
      const endCursor = timelines.page_info.end_cursor;
      return await recursiveFetchPosts({
        client,
        username,
        endCursor,
        edges: result,
        lastRow,
      });
    }
    if (posts.length === 0) {
      return [];
    }
    if (moment(posts[posts.length - 1].idate, 'YYYY-MM-DD HH:mm:ss').isBefore(lastDate)) {
      return result.filter(v => moment(v.idate, 'YYYY-MM-DD HH:mm:ss').isSameOrAfter(lastDate) && v.href !== lastURL);
    }
    return result.filter(v => moment(v.idate, 'YYYY-MM-DD HH:mm:ss').isSameOrAfter(lastDate) && v.href !== lastURL);
  }catch(err){
    await slack.error({ title: `fetchPosts 에러 - ${username}`, text: err.message });
    if(err.statusCode === 404) {
      return [];
    }
  }
}

async function fetchProfileImage({ client, sns_id }){
  try{
    const user = await client.getUserByUsername({ username : sns_id });
    return user && user.profile_pic_url_hd;
  }catch(err){
    await slack.error({ title: `fetchProfileImage 에러 - ${sns_id}`, text: err.message });
    return null;
  }
}

module.exports = {
  fetchProfileImage,
  recursiveFetchPosts
};
