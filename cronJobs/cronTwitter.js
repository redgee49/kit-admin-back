const moment = require('moment');

const { crawl, fetchProfileImage } = require('./twitter');
const pool = require('../config/muzSQL');

const { findAllArtist, findLastRow } = require('../query/CRAWL_QUERY');
const { insertPost, updateProfileImage } = require('../utils/insertSNS');

const slack = require('../utils/instaSlack');

require('moment-timezone');
moment.tz.setDefault('Asia/Seoul');

(async () => {
  const conn = await pool.getConnection();

  const [artistList] = await conn.query(findAllArtist, ['twitter']);

  console.log(`${moment().format('YYYY-MM-DD HH:mm:ss')} twitter crawling 시작`);

  const finished = [];

  for (const artist of artistList) {
    try{
      const profile_url = await fetchProfileImage({ screen_name : artist.sns_id });
      await updateProfileImage({ seq : artist.seq, sns_enum : 'twitter', url : profile_url });
      
      const [lastRow] = await conn.query(findLastRow, [artist.seq, 'twitter']);
      const exist = finished.find((v) => v.sns_id === artist.sns_id && !artist.filtering_search);

      let crawledData = (exist && exist.data) || await crawl({ 
        user_id: artist.sns_id,
        since_id: lastRow.length && lastRow[0].href,
        idate: (lastRow.length && lastRow[0].idate) || artist.filtering_search ? '2022-01-16' : '2020-12-31',
      }) || [];

      if(artist.filtering_search === 1) {
        let temp = crawledData.length;
        crawledData = crawledData.filter((item) => item.description.includes(artist.artist_nick_kr) || item.description.includes(artist.artist_nick_en));
        console.log(`필터링 결과 데이터 건수 : ${temp - crawledData.length}`);
      }

      for (const row of crawledData.reverse()) {
        await insertPost({ post: row, artist });
      }

      process.env.NODE_ENV === 'production' 
        ? (exist && crawledData.length > 0 ? console.log('\x1b[36m%s\x1b[0m', `중복 아티스트: ${artist.artist_nick_kr} ${crawledData.length} 완료`)
            : console.log(`${moment().format('YYYY-MM-DD HH:mm:ss')} ${artist.artist_nick_kr} - ${crawledData.length} 완료`))
        : (exist ? console.log('\x1b[36m%s\x1b[0m', `중복 아티스트: ${artist.artist_nick_kr} ${crawledData.length} 완료`)
            : console.log(`${moment().format('YYYY-MM-DD HH:mm:ss')} ${artist.artist_nick_kr} - ${crawledData.length} 완료`))

      finished.push({ sns_id : artist.sns_id, data : crawledData });
    }catch(err){
      console.error(err);
      await slack.error({ title: `${artist.artist_nick_kr} 트윗 에러`, text: err.message });
      continue;
    }
  }
  await slack.info({ title : 'twitter 크롤링 완료', text: `크롤링 ${moment().format('YYYY-MM-DD HH:mm:ss')} 완료 \n` });
  process.exit(0);

})();
