const moment = require('moment');
const {
  getChannel,
  getPlaylistItems,
  arrange
} = require('./youtube');
const pool = require('../config/muzSQL');
const { updateProfileImage, insertPost } = require('../utils/insertSNS');

(async () => {
  const conn = await pool.getConnection();
  const [artists] = await conn.query(`SELECT * FROM sns_artist_nick WHERE sns_enum = 'youtube';`);

  for (const artist of artists) {
    const [lastVideo] = await conn.query(`SELECT MAX(idate) as 'publishedAt' FROM sns_contents_info WHERE user_id = ${artist.seq};`);
    const channel = await getChannel({ channelId: artist.auth_id });
    const fetchedPicName = channel.profilePic?.url;
    await updateProfileImage({ seq: artist.seq, sns_enum: 'youtube', url: fetchedPicName });

    const contents = await getPlaylistItems({ playlistId: channel.uploadListId });
    const needUpdate = contents.items.some((item) => !lastVideo[0].publishedAt || moment(lastVideo[0]?.publishedAt).isBefore(item.publishedAt));
    if(needUpdate) {
      while(true) {
        // 재귀
        if(contents?.nextPageToken) {
          const isOld = contents.items.every((item) => lastVideo[0].publishedAt && moment(item.publishedAt).isSameOrBefore(lastVideo[0].publishedAt));
          // 다음페이지를 탐색했을때 lastVideo의 시간보다 모두 일찍 올라온것들이라면 가져올 필요가 없으므로 재귀 종료
          if(isOld) break;
          const next = await getPlaylistItems({ playlistId: channel.uploadListId, nextPageToken: contents.nextPageToken });
          contents['nextPageToken'] = next.nextPageToken;
          contents.items = [...contents.items, ...next.items];
          if(lastVideo[0].publishedAt) {
            contents.items = contents.items.filter(c => moment(c.snippet.publishedAt).isAfter(lastVideo[0].publishedAt));
            // 마지막 비디오보다 늦게 올라온것들만 필터링
          }
        } else {
          break;
        }
      }
      if(artist.filtering_search) {
        contents.items = contents.items.filter(c => [c.snippet.title, c.snippet.description].includes(artist.artist_nick_en) || [c.snippet.title, c.snippet.description].includes(artist.artist_nick_kr));
        // 아티스트 이름 포함된 영상들만 필터링
      }
      const posts = arrange({ contents });
      for(const post of posts) {
        await insertPost({ artist, post, sns_enum: 'youtube' })
      }
    }
  }
  process.exit(0);
})();