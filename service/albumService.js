const pool = require('../config/mySQL');
const { albumInfo, User } = require('../models');

exports.albumByAppId = async ({ app_id }) => {
  const album = await albumInfo.findOne({
    where : {
      app_id
    }
  });
  return album;
};

exports.albumListByCompany = async({ company_name }) => {
  const result = await User.findOne({
    where : {
      nickname : company_name
    },
    raw: true
  });

  const albumList = await albumInfo.findAll({
    where : {
      company_id : id,
    }
  })
  return albumList;
};