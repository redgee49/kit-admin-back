const kihno = require('../config/kihno');
const { invalidList, invalidRankList, statusAndroid, statusAll, currentList, statusApple, createDeviceQuery } = require('../query/DEVICE_MANAGE_QUERY');

exports.getInvalidRankList = async({ startDate, endDate }) => {
    const conn = await kihno.getConnection();
    try{
        const q = invalidRankList({ startDate, endDate });
        const [result] = await conn.query(q);
        return result;
    }catch(err){
        console.error(err);
    }finally{
        conn.release();
    }
}

exports.getInvalidList = async({ startDate, endDate }) => {
    const conn = await kihno.getConnection();
    try{
        const q = invalidList({ startDate, endDate });
        const [result] = await conn.query(q);
        return result;
    }catch(err){
        console.error(err);
    }finally{
        conn.release();
    }
}

exports.getAndroid = async({ startDate, endDate }) => {
    const conn = await kihno.getConnection();
    try{
        const q = statusAndroid({ startDate, endDate });
        const [result] = await conn.query(q);
        return result;
    }catch(err){
        console.error(err);
    }finally{
        conn.release();
    }
}

exports.getApple = async({ startDate, endDate }) => {
    const conn = await kihno.getConnection();
    try{
        const q = statusApple({ startDate, endDate });
        const [result] = await conn.query(q);
        return result;
    }catch(err){
        console.error(err);
    }finally{
        conn.release();
    }
}

exports.getAllList = async({ startDate, endDate }) => {
    const conn = await kihno.getConnection();
    try{
        const q = statusAll({ startDate, endDate });
        const [result] = await conn.query(q);
        return result;
    }catch(err){
        console.error(err);
    }finally{
        conn.release();
    }
}

exports.getCurrentList = async() => {
    const conn = await kihno.getConnection();
    try{
        const q = currentList();
        const [result] = await conn.query(q);
        return result;
    }catch(err){
        console.error(err);
    }finally{
        conn.release();
    }
}

exports.createDevice = async(values) => {
    const conn = await kihno.getConnection();
    try{
        const q = createDeviceQuery(values);
        await conn.query(q);
        await conn.query(`INSERT INTO KNS_SYNC_INFO(queryTxt, regionNm, USE_YN) VALUES(?, 'SeoulAdmin', 'Y');`, [q]);
    }catch(err){
        console.error(err);
        throw err;
    }finally{
        conn.release();
    }
}