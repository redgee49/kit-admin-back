
const muz = require('../config/muzSQL');
const pool = require('../config/mySQL');
const query = require('../query/SNS_QUERY');

function groupingSnsPosts({ posts }) {
  const set = new Set();
  posts.forEach(p => set.add(String(p.content_id + p.sns_id)));

  return Array.from(set).map(multikey => {
    const targetPost = posts.find(p => String(p.content_id + p.sns_id) === multikey);
    const grouped = posts.filter(p => String(p.content_id + p.sns_id) === multikey);
    const imageURLs = grouped.map(post => {
      if(!post.src)
        return null;
      return post.src.includes('https') ? post.src : `https://s3.ap-northeast-2.amazonaws.com/sns-seoul.muzlive.com/${post.src}`
    });
    targetPost.src = imageURLs.filter(v => v);

    if(targetPost.thumbnail){
      if(!targetPost.thumbnail.includes('https')){
        targetPost.thumbnail = `https://s3.ap-northeast-2.amazonaws.com/sns-seoul.muzlive.com/${targetPost.thumbnail}`;
      }
    }

    return targetPost;
  });
}

exports.snsList = async ({ sns_enum, lasttime }) => {
  const conn = await muz.getConnection();
  try{
    const [result] = await conn.query(query.wholeTimeLineBySNS, [sns_enum, lasttime]);
    return groupingSnsPosts({ posts: result });
  }catch(err){
    console.log(err);
    return err;
  }
};

exports.snsArtistList = async ({ sns_enum, page = 1 }) => {
  const conn = await muz.getConnection();
  try{
    const displayCountPerPage = 100;
    const [result] = await conn.query(query.wholeArtistList, [sns_enum, (page - 1) * displayCountPerPage, page * displayCountPerPage ]);
    return result;
  }catch(err){
    console.log(err);
    return err;
  }
};
exports.snsRecent = async ({ app_grp }) => {
  const conn = await muz.getConnection();
  try{
    const [result] = await conn.query(query.recent, [app_grp]);
    return result;
  }catch(err){
    console.log(err);
    return err;
  }
};

const sqlUpdate = async ({ app_id, company, artist }) => {
  const conn = await pool.getConnection();
  try {
    let id = 0;
    const [companyExist] = await conn.query(
      `SELECT * FROM company WHERE company='${company.split('_')[0].trim()}' AND team = '${company.trim()}' AND artist = '${artist}'`,
    );
    if (companyExist.length === 0) {
      const [result] = await conn.query(
        `INSERT INTO company(company, team, artist) values('${company.split('_')[0].trim()}', '${company.trim()}', '${artist}');`,
      );
      id = result.insertId;
    } else {
      id = companyExist[0].id;
    }

    const [albumExist] = await conn.query(`SELECT * FROM album WHERE company_id = '${id}' AND app_id = '${app_id}'`);
    if (albumExist.length === 0) {
      await conn.query(`INSERT INTO album(app_id, company_id) values('${app_id}', '${id}');`);
    }
  } catch (e) {
    console.log(`sqlUpdate${e}`);
  } finally {
    conn.release();
  }
};

// exports.albumModify = async ({ app_id, companies }) => {
//   const appIds = app_id.split(',');
//   const companyList = companies.split(',');

//   for (const id of appIds) {
//     for (const company of companyList) {
//       const temp = await albumInfo.findOne({
//         where : {
//           app_id : id,
//         }
//       });
//       const tempCompany = temp.company.replace(/,$/, '');
//       if (tempCompany.includes(company)) {
//         await sqlUpdate({ app_id: id, company, artist: temp.artist_nm });
//       } else {
//          await albumInfo.updateOne({ app_id: id }, { $set: { company: `${tempCompany},${company}` } });
//         await sqlUpdate({ app_id: id, company, artist: temp.artist_nm });
//       }
//     }
//   }
// };


