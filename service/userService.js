const { User } = require("../models");

exports.getUserList = async () => {
  const documents = await User.findAll();
  documents.unshift({ username: '미등록된 앨범' });
  return documents;
};
