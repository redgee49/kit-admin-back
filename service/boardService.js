const muz = require('../config/muzSQL');
const kihno = require('../config/kihno');
const voc = require('../config/voc');
const moment = require('moment');

exports.allArtistBoard = async({ currentPage = 1, disPlayPerCount = 10 }) => {
    const conn = await muz.getConnection();
    const kihno_conn = await kihno.getConnection();
    try{ 
        const [posts] = await conn.query(`
        SELECT concat(content.content_id, content.board_id) as 'key', content.app_id, content.group_id as 'tag', content.content_id, content.board_id, content.group_id, content.content_desc, content.idate,
            (select count(*) from FanKit_comment_info where board_id = content.board_id AND content_id = content.content_id) as 'comment_count',
            profile.nickname,
            media.file_url,
            content.content_delete, content.order_num
                FROM (
                    select content_id, board_id, group_id, content_desc, idate, user_id, content_delete, order_num, app_id
                    from FanKit_contents_info
                    where board_id not like 'notics%'
                    order by idate desc
                    LIMIT ${(currentPage - 1) * disPlayPerCount}, ${disPlayPerCount}
                ) content
                    LEFT OUTER JOIN (select board_id, content_id, file_url from SNS_media_info_user group by board_id, content_id) media
                        ON content.board_id = media.board_id AND content.content_id = media.content_id
                    LEFT OUTER JOIN FanKit_profile_info profile
                        ON content.user_id = profile.user_id AND content.group_id = profile.group_id
            order by idate desc;
        `);

        const notics = [];

        const [boardResult] = await conn.query(`
            SELECT count(*) as 'totalCount'
                FROM FanKit_contents_info;
        `);

        const { totalCount } = boardResult[0];
        currentPage === 0 && posts.unshift(...notics);

        return {
            artist_name : '전체 아티스트',
            totalCount,
            posts,
        }
    }catch(err){
        throw err;
    }finally{
        conn.release();
        kihno_conn.release();
    }
}

exports.boardList = async ({ group_id, currentPage = 1, disPlayPerCount = 10 }) => {
    const conn = await muz.getConnection();
    const kihno_conn = await kihno.getConnection();
    try{

        const [artist_info] = await kihno_conn.query(`
            SELECT artist_nm_kr, img_albumCoverNum
            FROM kihno_app_info
            WHERE app_grp = ?
            ORDER BY idate desc
            LIMIT 0, 1;
        `, [group_id]);

        const [boards] = await conn.query(`
            SELECT board_id, title
            FROM FanKit_board_info 
            WHERE group_id = ?
        `, [group_id]);
        if(boards){ 
            const free_board = boards.find((v => v.title === '자유게시판'));
            // 자유게시판 아이디
            const message_board = boards.find((v => v.title.includes('to.')));
            // to 게시판 아이디
            // 공지사항

            const [posts] = await conn.query(`
                SELECT concat(content.content_id, content.board_id) as 'key', content.app_id, content.content_id, content.board_id, content.group_id, content.content_desc, content.idate,
                    (select count(*) from FanKit_comment_info where board_id = content.board_id AND content_id = content.content_id) as 'comment_count',
                    profile.nickname,
                    media.file_url,
                    content.content_delete, content.order_num
                            FROM (
                                select content_id, board_id, group_id, content_desc, idate, user_id, content_delete, order_num, app_id
                                from FanKit_contents_info
                                where group_id = '${group_id}'AND board_id not like 'notics%'
                                order by idate desc
                                LIMIT ${(currentPage - 1) * disPlayPerCount}, ${disPlayPerCount}
                            ) content
                                LEFT OUTER JOIN (select board_id, content_id, file_url from SNS_media_info_user group by board_id, content_id) media
                                    ON content.board_id = media.board_id AND content.content_id = media.content_id
                                LEFT OUTER JOIN FanKit_profile_info profile
                                    ON content.user_id = profile.user_id AND content.group_id = profile.group_id
                                order by idate desc;
            `);

            const [notics] = await conn.query(`
                SELECT concat(content.content_id, content.board_id) as 'key', content.app_id, content.content_id, content.board_id, content.group_id, content.content_desc, content.idate,
                (select count(*) from FanKit_comment_info where board_id = content.board_id AND content_id = content.content_id) as 'comment_count',
                profile.nickname,
                media.file_url,
                content.content_delete, content.order_num
                    FROM (
                        select content_id, board_id, group_id, content_desc, idate, user_id, content_delete, order_num, app_id
                        from FanKit_contents_info
                        where group_id = '${group_id}' AND board_id like 'notics%' AND order_num > 0
                        order by idate desc
                        LIMIT ${(currentPage - 1) * disPlayPerCount}, ${disPlayPerCount}
                    ) content
                        LEFT OUTER JOIN (select board_id, content_id, file_url from SNS_media_info_user group by board_id, content_id) media
                            ON content.board_id = media.board_id AND content.content_id = media.content_id
                        LEFT OUTER JOIN FanKit_profile_info profile
                            ON content.user_id = profile.user_id AND content.group_id = profile.group_id
                        order by idate desc;`);

            const [boardResult] = await conn.query(`
                SELECT count(*) as 'totalCount'
                    FROM FanKit_contents_info 
                WHERE group_id = '${group_id}' AND board_id not like 'notics%';
            `);

            const { totalCount } = boardResult[0];
            currentPage === 0 && posts.unshift(...notics);

            return {
                artist_name : artist_info[0].artist_nm_kr,
                cover : artist_info[0].img_albumCoverNum,
                board_id : free_board.board_id,
                totalCount,
                posts : posts.map((v) => {
                    if(v.board_id === message_board.board_id){
                        v.tag = message_board.title;
                    }else if(v.board_id === free_board.board_id){
                        v.tag = '자유게시판';
                    }else{
                        v.tag = boards.find((row) => v.board_id === row.board_id).title;
                    }
                    return v;
                }),
            }
        }
    }catch(err){
        throw err;
    }finally{
        conn.release();
        kihno_conn.release();
    }
}

exports.boardSearch = async ({ group_id, currentPage = 1, disPlayPerCount = 10, q, type = 'content' }) => {
    if(type === 'content'){
        return await boardContentSearch({ group_id, currentPage, disPlayPerCount, q });
    }
    return await boardUserSearch({ group_id, currentPage, disPlayPerCount, q });
    // nickname search => type : 'user'
}

async function boardContentSearch({ group_id, currentPage = 1, disPlayPerCount = 10, q }) {
    const conn = await muz.getConnection();
    const kihno_conn = await kihno.getConnection();
    try{

        const [artist_info] = await kihno_conn.query(`
            SELECT artist_nm_kr, img_albumCoverNum
            FROM kihno_app_info
            WHERE app_grp = ?
            ORDER BY idate desc
            LIMIT 0, 1;
        `, [group_id]);

        const [boards] = await conn.query(`
            SELECT board_id, title
            FROM FanKit_board_info 
            WHERE group_id = ?
        `, [group_id]);
        if(boards){ 
            const free_board = boards.find((v => v.title === '자유게시판'));
            // 자유게시판 아이디
            const message_board = boards.find((v => v.title.includes('to.')));
            // to 게시판 아이디
            // 공지사항

            const [posts] = await conn.query(`
                SELECT concat(content.content_id, content.board_id) as 'key', content.app_id, content.content_id, content.board_id, content.group_id, content.content_desc, content.idate,
                    (select count(*) from FanKit_comment_info where board_id = content.board_id AND content_id = content.content_id) as 'comment_count',
                    profile.nickname,
                    media.file_url,
                    content.content_delete, content.order_num
                            FROM (
                                select content_id, board_id, group_id, content_desc, idate, user_id, content_delete, order_num, app_id
                                from FanKit_contents_info
                                where group_id = '${group_id}' AND board_id not like 'notics%' AND content_desc like '%${q}%'
                                order by idate desc
                                LIMIT ${(currentPage - 1) * disPlayPerCount}, ${disPlayPerCount}
                            ) content
                                LEFT OUTER JOIN (select board_id, content_id, file_url from SNS_media_info_user group by board_id, content_id) media
                                    ON content.board_id = media.board_id AND content.content_id = media.content_id
                                LEFT OUTER JOIN FanKit_profile_info profile
                                    ON content.user_id = profile.user_id AND content.group_id = profile.group_id
                                order by idate desc;
            `);

            const [boardResult] = await conn.query(`
                SELECT count(*) as 'totalCount'
                    FROM FanKit_contents_info 
                WHERE group_id = '${group_id}' AND board_id not like 'notics%' AND content_desc like '%${q}%';
            `);

            const { totalCount } = boardResult[0];

            return {
                artist_name : artist_info[0].artist_nm_kr,
                cover : artist_info[0].img_albumCoverNum,
                board_id : free_board.board_id,
                totalCount,
                posts : posts.map((v) => {
                    if(v.board_id === message_board.board_id){
                        v.tag = message_board.title;
                    }else if(v.board_id === free_board.board_id){
                        v.tag = '자유게시판';
                    }else{
                        v.tag = boards.find((row) => v.board_id === row.board_id).title;
                    }
                    return v;
                }),
            }
        }
    }catch(err){
        throw err;
    }finally{
        conn.release();
        kihno_conn.release();
    }
}

async function boardUserSearch({ group_id, currentPage = 1, disPlayPerCount = 10, q }) {
    const conn = await muz.getConnection();
    const kihno_conn = await kihno.getConnection();
    try{

        const [artist_info] = await kihno_conn.query(`
            SELECT artist_nm_kr, img_albumCoverNum
            FROM kihno_app_info
            WHERE app_grp = ?
            ORDER BY idate desc
            LIMIT 0, 1;
        `, [group_id]);

        const [boards] = await conn.query(`
            SELECT board_id, title
            FROM FanKit_board_info 
            WHERE group_id = ?
        `, [group_id]);
        if(boards){ 
            const free_board = boards.find((v => v.title === '자유게시판'));
            // 자유게시판 아이디
            const message_board = boards.find((v => v.title.includes('to.')));
            // to 게시판 아이디
            // 공지사항

            const [posts] = await conn.query(`
            SELECT concat(content.content_id, content.board_id) as 'key', content.content_id, content.board_id, content.group_id, content.content_desc, content.idate,
                (SELECT count(*) from FanKit_comment_info where board_id = content.board_id AND content_id = content.content_id) as 'comment_count',
                content.user_id,
                profile.nickname,
                media.file_url,
                content.content_delete,
                content.order_num
                    FROM (
                        SELECT c.content_id, c.board_id, c.group_id, c.content_desc, c.idate, c.user_id, c.content_delete, c.order_num, p.nickname
                        FROM (SELECT content_id, board_id, group_id, content_desc, idate, user_id, content_delete, order_num
                        from FanKit_contents_info WHERE group_id = '${group_id}') c LEFT OUTER JOIN FanKit_profile_info p ON c.user_id = p.user_id
                        WHERE nickname like '%${q}%'
                        ORDER BY idate desc
                        LIMIT ${(currentPage - 1) * disPlayPerCount}, ${disPlayPerCount}
                    ) content
                        LEFT OUTER JOIN (SELECT board_id, content_id, file_url from SNS_media_info_user group by board_id, content_id) media
                            ON content.board_id = media.board_id AND content.content_id = media.content_id
                        LEFT OUTER JOIN FanKit_profile_info profile
                            ON content.user_id = profile.user_id AND content.group_id = profile.group_id
                        order by idate desc;
            `);

            const [boardResult] = await conn.query(`
                SELECT count(*) as 'totalCount'
                    FROM FanKit_contents_info c LEFT OUTER JOIN FanKit_profile_info p ON c.user_id = p.user_id
                WHERE c.group_id = '${group_id}' AND c.board_id not like 'notics%' AND p.nickname like '%${q}%';
            `);

            const { totalCount } = boardResult[0];

            return {
                artist_name : artist_info[0].artist_nm_kr,
                cover : artist_info[0].img_albumCoverNum,
                board_id : free_board.board_id,
                totalCount,
                posts : posts.map((v) => {
                    if(v.board_id === message_board.board_id){
                        v.tag = message_board.title;
                    }else if(v.board_id === free_board.board_id){
                        v.tag = '자유게시판';
                    }else{
                        v.tag = boards.find((row) => v.board_id === row.board_id).title;
                    }
                    return v;
                }),
            }
        }
    }catch(err){
        throw err;
    }finally{
        conn.release();
        kihno_conn.release();
    }
}

exports.allBoardSearch = async ({ group_id, currentPage = 1, disPlayPerCount = 10, q }) => {
    const conn = await muz.getConnection();
    const kihno_conn = await kihno.getConnection();
    try{ 
        const [posts] = await conn.query(`
        SELECT concat(content.content_id, content.board_id) as 'key', content.app_id, content.group_id as 'tag', content.content_id, content.board_id, content.group_id, content.content_desc, content.idate,
            (select count(*) from FanKit_comment_info where board_id = content.board_id AND content_id = content.content_id) as 'comment_count',
            profile.nickname,
            media.file_url,
            content.content_delete, content.order_num
                FROM (
                    select content_id, board_id, group_id, content_desc, idate, user_id, content_delete, order_num, app_id
                    from FanKit_contents_info
                    where board_id not like 'notics%' AND content_desc like '%${q}%'
                    order by idate desc
                    LIMIT ${(currentPage - 1) * disPlayPerCount}, ${disPlayPerCount}
                ) content
                    LEFT OUTER JOIN (select board_id, content_id, file_url from SNS_media_info_user group by board_id, content_id) media
                        ON content.board_id = media.board_id AND content.content_id = media.content_id
                    LEFT OUTER JOIN FanKit_profile_info profile
                        ON content.user_id = profile.user_id AND content.group_id = profile.group_id
            order by idate desc;
        `);

        const notics = [];

        const [boardResult] = await conn.query(`
            SELECT count(*) as 'totalCount'
                FROM FanKit_contents_info;
        `);

        const { totalCount } = boardResult[0];
        currentPage === 0 && posts.unshift(...notics);

        return {
            artist_name : '전체 아티스트',
            totalCount,
            posts,
        }
    }catch(err){
        throw err;
    }finally{
        conn.release();
        kihno_conn.release();
    }
}

exports.boardDelete = async ({ ids }) => {
    const conn = await muz.getConnection();

    try{

        const condition = ids.map((v) => { 
                return `(board_id = '${v.board_id}' AND content_id = ${v.content_id}) `
            }).join('OR ')

        await conn.query(`
            UPDATE FanKit_contents_info SET content_delete = 'Y'
            WHERE ${condition}
        `);

        return { result : 'SUCCESS', ids };
    }catch(err){
        throw err;
    }finally{
        conn.release();
    }
}

exports.adminBoard = async () => {
    const conn = await muz.getConnection();

    try{

        const [posts] = await conn.query(`
            SELECT concat(c.content_id, c.board_id) as 'key', c.app_id, c.group_id, c.content_id, c.board_id, c.group_id, c.content_desc, c.idate, m.file_url, c.content_delete, c.order_num
                FROM FanKit_contents_info c LEFT OUTER JOIN SNS_media_info_user m ON c.board_id = m.board_id AND c.content_id = m.content_id
            WHERE c.user_id = 'muzlive'
            GROUP BY c.content_id, c.board_id
            ORDER BY c.idate desc;
        `);

        const [comments] = await conn.query(`
        SELECT c.board_id, c.content_id, c.comment, c.idate, p.nickname, c.comment_id
            FROM FanKit_comment_info c LEFT OUTER JOIN FanKit_profile_info p ON c.user_id = p.user_id AND c.group_id = p.group_id
        WHERE (board_id, content_id) 
                    in (SELECT board_id, content_id
                            FROM FanKit_contents_info
                        WHERE user_id = 'muzlive')
        `);

        return {
            posts : posts.map((v) => {
                const targetComments = comments.filter((c) => {
                    return (v.board_id === c.board_id) && (v.content_id === c.content_id)
                });

                if(targetComments.length){
                    v.comments = targetComments;
                }

                return v;
            })
        }

        
    }catch(err){
        throw err;
    }finally{
        conn.release();
    }
}

exports.commentDelete = async ({ board_id, content_id, comment_id }) => {
    const conn = await muz.getConnection();
    try{
        await conn.query(`
            DELETE FROM FanKit_comment_info 
            WHERE board_id = ? AND content_id = ? AND comment_id = ?
        `, [ board_id, content_id, comment_id ]);

        return {
            board_id,
            content_id,
            comment_id,
            result : 'SUCCESS'
        }

    }catch(err){
        throw err;
    }finally {
        conn.release();
    }
}

exports.noticeInsert = async ({ app_id, artists, contents, imagePath }) => {
    const conn = await muz.getConnection();
    try{
        
        for(const artist of artists){
            const [boards] = await conn.query(`
                SELECT board_id
                FROM FanKit_board_info
                WHERE group_id = ? AND board_id like 'notics%' AND board_id is not null
            `, [artist.group_id])

            if(boards.length === 0){
                continue;
            }
            const notice_board_id = boards[0].board_id;
            // 그룹아이디로 공지사항 게시판 board_id를 찾습니다.

            const [exist_notices] = await conn.query(`
                SELECT app_id, content_id, max(order_num) as order_num
                FROM FanKit_contents_info 
                WHERE board_id = ? AND group_id = ?
                ORDER BY content_id;
            `, [notice_board_id, artist.group_id])

            let content_id = 2147483647;
            if(exist_notices.length > 0){
                content_id = Number(exist_notices[0].content_id);
            }
            let order_num = 0;
            if(exist_notices.length > 0){
                order_num = Number(exist_notices[0].order_num);
            }else{
                order_num = 1;
            }

            content_id = content_id - 1;
            await conn.query(`
                INSERT INTO FanKit_contents_info
                    (group_id, user_id, board_id, content_id, order_num, content_desc, file_jong, jarang, view_yn, view_count, content_delete)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            `, [artist.group_id, 'muzlive', notice_board_id, content_id, order_num + 1, contents, 'image', 'Y', 'Y', 0, 'N'])
            // 해당 board_id 를 이용해 컨텐츠 테이블로 공지사항 본문을 insert 시킵니다.

            for(const [index, image] of Object.entries(imagePath)){

                const image_url = image.replace('https://s3.ap-northeast-2.amazonaws.com/sns-seoul.muzlive.com/', '');
                

                await conn.query(`
                    INSERT INTO SNS_media_info_user
                        (user_id, board_id, content_id, file_url, file_order, media_type)
                    VALUES (?, ?, ?, ?, ?, ?)
                `, ['muzlive', notice_board_id, content_id, image_url, Number(index) + 1, 'image'])
            }
            // 이미지가 여러개일 수도 있기 때문에 갯수만큼 insert
        } 
        
        return {
            result : 'SUCCESS',
        }
    }catch(err){
        throw err;
    }finally {
        conn.release();
    }
}

exports.comment = async({ app_id, board_id, content_id, group_id, contents }) => {

    const conn = await muz.getConnection();
    try{

        const [result] = await conn.query(`
            SELECT comment_id, upper_no, app_id, group_id
            FROM FanKit_comment_info
            WHERE board_id = ?
            ORDER BY idate desc;
        `, [board_id]);

        let comment_id = 0;
        let upper_no = 0;
        if(result.length > 0){
            comment_id = Number(result[0].comment_id) + 1;
            upper_no = Number(result[0].upper_no) - 1;
        }

        console.log(['muzlive', board_id, content_id, comment_id, upper_no, 0, 0, contents, app_id, group_id])

        await conn.query(`
            INSERT INTO FanKit_comment_info 
                        (user_id, board_id, content_id,  comment_id, upper_no, ref_sort, ref_step, comment, app_id, group_id)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        `, ['muzlive', board_id, content_id, comment_id, upper_no, 0, 0, contents, app_id, group_id]);

        return {
            content_id,
            board_id,
            comment: contents,
            nickname : 'KiTTOR MASTER',
            idate : moment().format('YYYY-MM-DD HH:mm:ss'),
            comment_id,
        }
    }catch(err){
        throw err;
    }finally {
        conn.release();
    }
}

exports.getComments = async({ board_id, content_id }) => {

    const conn = await muz.getConnection();
    try{

        const [comments] = await conn.query(`
            SELECT board_id, content_id, u.nickname, comment_id, comment, c.idate
            FROM FanKit_comment_info c LEFT OUTER JOIN FanKit_profile_info u ON u.user_id = c.user_id
            WHERE c.content_id = ? AND c.board_id = ?
            group by comment_id;
        `, [content_id, board_id]);

        return comments;
    }catch(err){
        throw err;
    }finally {
        conn.release();
    }
}

exports.likePost = async({ content_id, board_id, group_id, app_id }) => {
    const conn = await muz.getConnection();
    try{
        await conn.query(`
            INSERT INTO FanKit_like_info
                    (user_id, board_id, content_id, app_id, group_id)
                VALUES (?, ?, ?, ?, ?)
        `, ['muzlive', board_id, content_id, app_id, group_id]);

        return {
            content_id,
            board_id,
            app_id,
            group_id
        }
    }catch(err){
        throw err;
    }finally {
        conn.release();
    }
}

exports.vocList = async({ page = 1 }) => {
    const conn = await voc.getConnection();
    try{
        const [result] = await conn.query(`
            SELECT group_id, href, post_title as 'title', post_text as 'description', post_type, upload_date
            FROM VOC_bucket
            ORDER BY upload_date desc
            LIMIT ?, ?
        `, [(page - 1) * 300, page * 300]);
        return result;
    }catch(err){
        throw err;
    }finally{
        conn.release();
    }
}

exports.adminModify = async({ board_id, content_id, checked }) => {
    const conn = await muz.getConnection();

    try{
        
        await conn.query(`
            UPDATE FanKit_contents_info SET order_num = ?
            WHERE board_id = ? AND content_id = ?
        `, [checked ? 1 : 0, board_id, content_id]);

        return await this.adminBoard();
    }catch(err){
        throw err;
    }finally{
        conn.release();
    }
}