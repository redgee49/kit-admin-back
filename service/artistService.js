const pool = require('../config/muzSQL');
const kihno = require('../config/kihno');
const query = require('../query/ARTIST_QUERY');
const instagram = require('instatouch');

exports.findGroupsList = async () => {
  const conn = await kihno.getConnection();

  try {
    const [list] = await conn.query(query.artistListGroupByGroupName);
    return list;
  } catch (e) {
    console.log(e);
  } finally {
    conn.release();
  }
};

exports.allArtistsInGroup = async ({ group_name }) => {
  const conn = await pool.getConnection();

  try {
    const [list] = await conn.query(query.allArtistByGroupName, [group_name, group_name]);
    return list;
  } catch (e) {
    console.log(e);
  } finally {
    conn.release();
  }
};

exports.addArtistToGroup = async ({ artist_name, group_name, username, sns_enum }) => {
  const conn = await pool.getConnection();
  try {
    if(sns_enum === 'instagram'){
      const user_info = await instagram.getUserMeta(username);
      const [result] = await conn.query(query.instaAddArtistToGroup, [`${group_name} ${artist_name || ''}`, artist_name || group_name, group_name, username, sns_enum, user_info.graphql.user.id]);
      return result;
    }else{
      const [result] = await conn.query(query.addArtistToGroup, [`${group_name} ${artist_name || ''}`, artist_name || group_name, group_name, username, sns_enum]);
      return result;
    }
  } catch (e) {
    console.log(e);
  } finally {
    console.info(`${artist_name} - ${username}(${sns_enum}) 추가`);
    conn.release();
  }
};

exports.botInsertArtist = async ({ kr_name, en_name, group_name, username, sns_enum }) => {
  const conn = await pool.getConnection();
  try {
    if(sns_enum === 'instagram'){
      const user_info = await instagram.getUserMeta(username);
      const [result] = await conn.query(query.botInsertArtistQuery, [kr_name, en_name, group_name, username, sns_enum, user_info.graphql.user.id]);
      return result;
    }else{
      const [result] = await conn.query(query.botInsertArtistQuery, [kr_name, en_name, group_name, username, sns_enum, null]);
      return result;
    }
  } catch (e) {
    console.log(e);
  } finally {
    console.info(`${kr_name} - ${en_name}(${sns_enum}) 추가`);
    conn.release();
  }
};

exports.released = async() => {
  const kihno_conn = await kihno.getConnection();
  const conn = await pool.getConnection();
  try{
    const [albumList] = await kihno_conn.query(query.currentAlbumList);
    const [groups] = await conn.query(query.distinctGroupName);

    const target = groups.map((v) => {
      return v.app_grp;
    })

    const result = albumList.filter((v) => {
      return !target.includes(v.app_grp);
    })
    return result;
  }catch(e){
    console.log(e);
  }
  
  
}