const express = require('express');

const router = express.Router();
const { getUserList } = require('../service/userService');
const { isLoggedIn } = require('../middlewares');

router.get('/list', isLoggedIn, async (req, res, next) => {
  try {
    const result = await getUserList();
    res.json(result);
  } catch (err) {
    console.error(err);
    next(err);
  }
});

module.exports = router;
