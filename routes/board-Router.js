const express = require('express');
const router = express.Router();
const multer = require('multer');
const multerS3 = require('multer-s3');
const path = require('path');
const { s3, BUCKENT_NAME } = require('../utils');
const moment = require('moment');

require('dotenv').config(path.join(__dirname, '../.env'));

const { boardList, boardDelete, commentDelete, noticeInsert, comment, likePost, adminBoard, getComments, vocList, adminModify, allArtistBoard, boardSearch, allBoardSearch } = require('../service/boardService');
const { isLoggedIn } = require('../middlewares');

const upload = multer({
  storage : multerS3({
    s3,
    bucket : BUCKENT_NAME,
    key(req, file, cb) {
      cb(null,  `mp3filera/${moment().format('YYYYMMDD')}/${Date.now()}_${path.basename(file.originalname)}`)
    },
    limits: { fileSize: 20 * 1024 * 1024 }, // 20MB
  })
})

router.post('/images', isLoggedIn, upload.array('image'), async(req, res, next) => {
  try{
    res.json(req.files.map((v) => v.location));
  }catch(err){
    console.error(err);
    next(err);
  }
});

router.get('/voc/list', isLoggedIn, async(req, res, next) => {
  try{
    const result = await vocList({
      page : req.query.page
    })
    res.status(200).json(result);
  }catch(err){
    console.error(err);
    next(err);
  }
})

router.get('/admin/list', isLoggedIn, async(req, res, next) => {
  try{
    const result = await adminBoard();
    res.json(result);
  }catch(err){
    console.error(err);
    next(err);
  }
});

router.post('/admin/modify', isLoggedIn, async(req, res, next) => {
  try{
    const result = await adminModify({
      board_id : req.body.board_id,
      content_id : req.body.content_id,
      checked : req.body.checked,
    });
    res.json(result);
  }catch(err){
    console.error(err);
    next(err);
  }
});

router.post('/notice', isLoggedIn, async(req, res, next) => {
  try{
    const result = await noticeInsert({ 
      artists : req.body.artists,
      contents : req.body.contents,
      imagePath : req.body.imagePath
    });
    res.json(result);
  }catch(err){
    console.error(err);
    next(err);
  }
});

router.post('/like', isLoggedIn, async(req, res, next) => {
  try{
    const result = await likePost({ 
      board_id : req.body.board_id,
      content_id : req.body.content_id,
      group_id : req.body.group_id,
      app_id : req.body.app_id
    });
    res.json(result);
  }catch(err){
    console.error(err);
    next(err);
  }
});

router.get('/comment/:board_id/:content_id', isLoggedIn, async(req, res, next) => {
  try{
    const result = await getComments({
      board_id : req.params.board_id,
      content_id : req.params.content_id,
    })
    res.json(result);
  }catch(err){
    console.error(err);
    next(err);
  }
})

router.post('/comment', isLoggedIn, async(req, res, next) => {
  try{
    const result = await comment({ 
      app_id : req.body.app_id,
      board_id : req.body.board_id,
      content_id : req.body.content_id,
      contents : req.body.contents,
      group_id : req.body.group_id
    });
    res.json(result);
  }catch(err){
    console.error(err);
    next(err);
  }
});

router.post('/comment/delete', isLoggedIn, async(req, res, next) => {
  try{
    const result = await commentDelete({ 
      board_id : req.body.board_id,
      content_id : req.body.content_id,
      comment_id : req.body.comment_id
    });
    res.json(result);
  }catch(err){
    console.error(err);
    next(err);
  }
});

router.post('/delete', isLoggedIn, async(req, res, next) => {
    try{
      const result = await boardDelete({ ids : req.body.ids });
      res.json(result);
    }catch(err){
      console.error(err);
      next(err);
    }
});

router.get('/all/list', isLoggedIn, async(req, res, next) => {
  try {
    const result = await allArtistBoard({
      currentPage : req.query.page,
      disPlayPerCount : req.query.per
    });
    res.json(result);
  } catch (err) {
    console.error(err);
    next(err);
  }
})

router.get('/:group_id/list', isLoggedIn, async (req, res, next) => {
  try {
    const result = await boardList({ 
      group_id : req.params.group_id,
      currentPage : req.query.page,
      disPlayPerCount : req.query.per
    });
    res.json(result);
  } catch (err) {
    console.error(err);
    next(err);
  }
});

router.get('/all/search', isLoggedIn, async (req, res, next) => {
  try {
    const result = await allBoardSearch({ 
      group_id : req.params.group_id,
      currentPage : req.query.page,
      disPlayPerCount : req.query.per,
      q: req.query.q
    });
    res.json(result);
  } catch (err) {
    console.error(err);
    next(err);
  }
});

router.get('/:group_id/search', isLoggedIn, async (req, res, next) => {
  try {
    const result = await boardSearch({  
      group_id : req.params.group_id,
      currentPage : req.query.page,
      disPlayPerCount : req.query.per,
      q: req.query.q,
      type: req.query.type
    });
    res.json(result);
  } catch (err) {
    console.error(err);
    next(err);
  }
});




module.exports = router;
