const express = require('express');

const router = express.Router();
const { isLoggedIn } = require('../middlewares');

router.get('/image/:url', isLoggedIn, async (req, res, next) => {
  try {
    const result = await snsList({ sns_enum: req.params.sns_enum, lasttime : req.query.lasttime });
    res.json(result);
  } catch (err) {
    console.error(err);
    next(err);
  }
});

// router.get('/:sns_enum/artist', async(req, res) => {
//     try{
//       const result = await snsList({ sns_enum: req.params.sns_enum, page : req.query.page });
//       res.json(result);
//     }catch(e){
//         res.status(204);
//     }
// })

module.exports = router;
