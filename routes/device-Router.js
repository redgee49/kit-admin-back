const express = require('express');

const router = express.Router();
const { getInvalidList, getAndroid, getAllList, getCurrentList, getApple, createDevice } = require('../service/deviceService');

router.post('/create', async(req, res, next) => {
  try{
    await createDevice({
      ...req.body
    });
    res.send('finish !');
  }catch(err){
    console.error(err);
    next(err);
  }
})

router.get('/invalid/list', async (req, res, next) => {
  try {
    const result = await getInvalidList({
        startDate : req.query.startDate,
        endDate : req.query.endDate,
     })
    res.json(result);
  } catch (err) {
    console.error(err);
    next(err);
  }
});

router.get('/current/list', async (req, res, next) => {
  try {
    const result = await getCurrentList();
    res.json(result);
  } catch (err) {
    console.error(err);
    next(err);
  }
});

router.get('/status/android', async(req, res, next) => {
    try {
        const result = await getAndroid({
            startDate : req.query.startDate,
            endDate : req.query.endDate,
         })
        res.json(result);
      } catch (err) {
        console.error(err);
        next(err);
      }
})

router.get('/status/apple', async(req, res, next) => {
  try {
      const result = await getApple({
          startDate : req.query.startDate,
          endDate : req.query.endDate,
       })
      res.json(result);
    } catch (err) {
      console.error(err);
      next(err);
    }
})

router.get('/status/all', async(req, res, next) => {
    try {
        const result = await getAllList({
            startDate : req.query.startDate,
            endDate : req.query.endDate,
         })
        res.json(result);
      } catch (err) {
        console.error(err);
        next(err);
      }
})
module.exports = router;
