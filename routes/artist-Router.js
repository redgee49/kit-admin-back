const express = require('express');

const router = express.Router();

const { findGroupsList, addArtistToGroup, allArtistsInGroup, released } = require('../service/artistService');
const { isLoggedIn } = require('../middlewares');

router.get('/released', isLoggedIn, async (req, res, next) => {
  try{
    const result = await released();
    res.json(result);
  }catch(err){
    console.error(err);
    next(err);
  }
})

router.get('/groups', isLoggedIn, async (req, res, next) => {
  try {
    const groups = await Promise.all(await findGroupsList());
    res.json(groups);
  } catch (err) {
    console.error(err);
    next(err);
  }
});

router.get('/:group_name', isLoggedIn, async (req, res, next) => {
  try {
    const artists = await allArtistsInGroup({ group_name: req.params.group_name });
    res.json(artists);
  } catch (err) {
    console.error(err);
    next(err);
  }
});

router.post('/add', isLoggedIn, async (req, res, next) => {
  try {
    const result = await addArtistToGroup({
      artist_name: req.body.artist_name,
      group_name: req.body.group_name,
      username: req.body.user_id,
      sns_enum: req.body.sns_enum,
    });

    res.json(result);
  } catch (err) {
    console.error(err);
    next(err);
  }
});

module.exports = router;
