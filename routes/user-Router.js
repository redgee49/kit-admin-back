const express = require('express');
const router = express.Router();
const passport = require('passport');
const pool = require('../config/mySQL');

router.get('/', async (req, res, next) => {
  try {
    if (req.user) {
      const [fullUserWithOutPassword] = await pool.query(`
        SELECT id, user_id, email, nickname
        FROM Users
        WHERE id = '${req.user.id}';
      `)
      res.status(200).json(fullUserWithOutPassword[0]);
    } else {
      console.log('incorrect user info');
      res.status(200).json(null);
    }
  } catch (err) {
    console.error(err);
    next(err);
  }
});

router.post('/login', (req, res, next) => {
  passport.authenticate('local', (err, user, info) => {
    if (err) {
      console.error(err);
      return next(err);
    }
    if (info) {
      return res.status(403).send(info.reason);
    }
    return req.logIn(user, error => {
      if (error) {
        console.error(error);
        return next(error);
      }
      return res.json(user);
    });
  })(req, res, next);
});

module.exports = router;
