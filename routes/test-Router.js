const express = require('express');

const router = express.Router();

router.get('/ping', (req, res) => {
  res.send('tested ping ⚡️');
});

module.exports = router;
