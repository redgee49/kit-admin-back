const express = require('express');
const { albumByAppId, albumListByCompany } = require('../service/albumService');

const router = express.Router();
const { isLoggedIn } = require('../middlewares');

router.get('/list', isLoggedIn, async (req, res, next) => {
  try {
    const result = await albumListByCompany({ company_name : req.query.company_id  });
    res.json(result);
  } catch (err) {
    console.error(err)
    next(err);
  }
});

router.get('/:app_id', isLoggedIn, async (req, res, next) => { 
  try {
    const result = await albumByAppId({ app_id: req.params.app_id });
    res.json(result);
  } catch (err) {
    console.error(err)
    next(err);
  }
});

module.exports = router;
