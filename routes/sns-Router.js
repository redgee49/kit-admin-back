const express = require('express');

const router = express.Router();

const { snsList, snsRecent } = require('../service/snsService');
const { isLoggedIn } = require('../middlewares');

router.get('/recent/:app_grp', async(req, res, next) => {
  try{
    const result = await snsRecent({ app_grp: req.params.app_grp });
    res.json(result);
  }catch(err) {
    console.error(err);
    next(err);
  }
})

router.get('/:sns_enum/list', isLoggedIn, async (req, res, next) => {
  try {
    const result = await snsList({ sns_enum: req.params.sns_enum, lasttime : req.query.lasttime });
    res.json(result);
  } catch (err) {
    console.error(err);
    next(err);
  }
});

// router.get('/:sns_enum/artist', async(req, res) => {
//     try{
//       const result = await snsList({ sns_enum: req.params.sns_enum, page : req.query.page });
//       res.json(result);
//     }catch(e){
//         res.status(204);
//     }
// })

module.exports = router;
