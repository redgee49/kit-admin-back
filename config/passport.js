const passport = require('passport');
const { Strategy: LocalStrategy } = require('passport-local');
const path = require('path');
const pool = require('../config/mySQL');
const bcrypt = require('bcrypt');


require('dotenv').config({ path: path.join(__dirname, '../.env') });

module.exports = () => {
  passport.serializeUser((user, done) => {
    done(null, user.id);
  });

  passport.deserializeUser(async(id, done) => {
    try{
      const [user] = await pool.query(`SELECT * FROM Users WHERE id = ${id};`);
      return done(null, user[0]);
    }catch(error){
      console.error(error);
      return err.message;
    }
  });

  passport.use(
    new LocalStrategy(
      {
        usernameField: 'username',
        passwordField: 'password',
      },
      async(username, password, done) => {

        try{
          const [user] = await pool.query(`SELECT * FROM Users WHERE user_id = '${username}';`);

          if(!user.length){
            return done(null, false, { reason : '존재하지 않는 사용자입니다.'});
          }

          const result = await bcrypt.compare(password, user[0].password);
          if(result){
            return done(null, user[0]);
          }
          
          return done(null, false, { reason : '아이디와 비밀번호를 확인해주세요'});
        }catch(error){
          console.error(error);
          return done(error);
        }
      }
    )
  );
};
