const mysql = require('mysql2/promise');
const path = require('path');

require('dotenv').config({ path: path.join(__dirname, '../.env') });

const pool = mysql.createPool({
  host: process.env.KITINSIDE_SQL_URL,
  user: process.env.KITINSIDE_SQL_USER,
  password: process.env.KITINSIDE_SQL_PASSWORD,
  database: process.env.KITINSIDE_SQL_DB,
  connectionLimit: 100,
  charset: 'utf8mb4',
});

module.exports = pool;
