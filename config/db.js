const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '../.env') });

module.exports = {
  development: {
    username: process.env.KITINSIDE_SQL_USER,
    password: process.env.KITINSIDE_SQL_PASSWORD,
    database: process.env.KITINSIDE_SQL_DB,
    host: process.env.KITINSIDE_SQL_URL,
    port: 3306,
    dialect: 'mysql',
    dialectOptions: {
      decimalNumbers: true,
    },
    logging: false,
  },
  production: {
    username: process.env.KITINSIDE_SQL_USER,
    password: process.env.KITINSIDE_SQL_PASSWORD,
    database: process.env.KITINSIDE_SQL_DB,
    host: process.env.KITINSIDE_SQL_URL,
    port: 3306,
    dialect: 'mysql',
    dialectOptions: {
      decimalNumbers: true,
    },
    logging: false,
  },
};
