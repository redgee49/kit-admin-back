const mysql = require('mysql2/promise');
const path = require('path');

require('dotenv').config({ path: path.join(__dirname, '../.env') });

const connection = mysql.createPool({
  host: process.env.KIHNO_SQL_URL,
  user: process.env.KIHNO_SQL_USER,
  password: process.env.KIHNO_SQL_PASSWORD,
  database: process.env.KIHNO_SQL_DB,
  connectionLimit: 100,
  charset: 'utf8mb4',
});

module.exports = connection;
