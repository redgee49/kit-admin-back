const mysql = require('mysql2/promise');
const path = require('path');

require('dotenv').config({ path: path.join(__dirname, '../.env') });

const connection = mysql.createPool({
  host: process.env.VOC_SQL_URL,
  user: process.env.VOC_SQL_USER,
  password: process.env.VOC_SQL_PASSWORD,
  database: process.env.VOC_SQL_DB,
  connectionLimit: 100,
});

module.exports = connection;
