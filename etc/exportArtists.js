const pool = require('../config/mySQL');
const muzPool = require('../config/muzSQL');

(async () => {
  const conn = await pool.getConnection();
  const muz = await muzPool.getConnection();

  const [artists] = await conn.query(
    `SELECT * FROM kit_inside.SNS_ARTIST
         ORDER BY group_name, artist_name asc;`,
  );

  for (const row of artists) {
    await muz.query(
      `
            INSERT INTO TEST_SNS_artist_nick(artist_nick_kr, artist_nick_en, app_grp, sns_id, sns_enum,  auth_id)
            VALUES(?, ?, ?, ?, ?, ?)
        `,
      [row.artist_name, row.artist, row.group_name, row.user_id, row.sns_enum, row.auth_id || null],
    );
  }
})();
