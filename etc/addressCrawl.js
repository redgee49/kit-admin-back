const puppeteer = require('puppeteer');
const pool = require('../config/mySQL');
const albumListCollection = require('../models/albumListModel');

const waitor = {
  waitUntil: 'networkidle0',
};

const naver_query = 'https://search.naver.com/search.naver?sm=top_hty&fbm=1&ie=utf8&query=';
const SNS = [
  {
    name: '페이스북',
    address: 'https://www.facebook.com/',
    en_name: 'facebook',
  },
  {
    name: '트위터',
    address: 'https://twitter.com/',
    en_name: 'twitter',
  },
  {
    name: '인스타그램',
    address: 'https://www.instagram.com/',
    en_name: 'instagram',
  },
];

const crawlAllArtistSNS = async ({ sns_enum }) => {
  const conn = await pool.getConnection();

  const artistList = await albumListCollection.find().distinct('artist_nm');

  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  const data = [];
  const notExist = [];

  const targetSNS = SNS.find(v => v.name === sns_enum);

  for (const artist of artistList) {
    const temp = {};

    await page.goto(`${naver_query}${artist}`, waitor);

    try {
      await page.waitForSelector('.detail_profile > dd > a', { timeout: 2000 });

      const url = await page.evaluate(() => {
        const { href } = Array.from(document.querySelectorAll('.detail_profile > dd > a')).find(el => {
          return el.textContent.includes(targetSNS.name);
        });

        return href;
      });

      temp.artist_name = artist.replace(/'/g, "\\'");
      temp.user_id = url.replace(targetSNS.address, '');
      temp.sns_enum = targetSNS.en_name;

      data.push(temp);
    } catch (e) {
      notExist.push(artist);
      console.log(artist);
      continue;
    }
  }

  for (const el of data) {
    try {
      await conn.query(
        `INSERT INTO SNS_ARTIST(artist, artist_name, user_id, sns_enum) values ('${el.artist_name}', '${el.artist_name}', '${el.user_id}', '${el.sns_enum}')`,
      );
    } catch (e) {
      console.log(el.artist_name);
      continue;
    }
  }

  console.log(notExist);
};

(async () => {
  await Promise.all(
    ['인스타그램', '트위터', '페이스북'].map(async v => {
      await crawlAllArtistSNS(v);
    }),
  );
  console.log('전부 끝');
})();

module.exports = crawlAllArtistSNS;
