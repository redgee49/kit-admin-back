const pool = require('../config/mySQL');
const muzPool = require('../config/muzSQL');
const kihnoPool = require('../config/kihno');

const targetContentsTable = 'SNS_contents_info';
const targetMediaTable = 'SNS_media_info';
const targetArtistTable = 'SNS_artist_nick';

const INT_MAX_VALUE = 2147483647;

async function generateContentId({ conn, board_id }) {
  const [list] = await conn.query(`SELECT MIN(content_id) as content_id FROM ${targetContentsTable} WHERE board_id = ?`, [board_id]);
  if (list[0].content_id === null) {
    return INT_MAX_VALUE;
  }

  return list[0].content_id - 1;
}

async function generateMediaId({ conn }) {
  const [list] = await conn.query(`SELECT MIN(seq) as seq FROM ${targetMediaTable}`);
  if (list[0].seq === null) {
    return INT_MAX_VALUE;
  }

  return list[0].seq - 1;
}

async function fetchLastAppId({ artist }) {
  const conn = await kihnoPool.getConnection();
  try {
    const [res] = await conn.query(`SELECT app_id FROM kihno_app_info WHERE app_grp = ? ORDER BY idate;`, [artist.group_name]);
    return res[0].app_id;
  } catch (e) {
    console.log(e);
  } finally {
    conn.release();
  }
}

const exportPost = async ({ post, conn, muzConn }) => {
  try {
    const originID = post.id;
    // SNS_POST 테이블의 원본 id

    const [mediaList] = await conn.query(`SELECT * FROM SNS_CONTENTS WHERE post_id = ?`, [originID]);
    // 원본 POST와 연결된 Media들

    const [artistList] = await conn.query(`SELECT * FROM SNS_ARTIST WHERE id = ?`, [post.artist_id]);
    // 원본 POST와 연결된 아티스트 정보

    const artist = artistList[0];

    const [board_info] = await muzConn.query(`SELECT * FROM FanKit_board_info WHERE group_id = ? AND title = ?`, [artist.group_name, 'star']);

    const { board_id } = board_info[0];

    const [targetArtist] = await muzConn.query(`SELECT * FROM ${targetArtistTable} WHERE app_grp = ? AND sns_enum = ? AND artist_nick_kr = ?`, [
      artist.group_name,
      artist.sns_enum,
      artist.artist_name,
    ]);

    const artist_nick = targetArtist[0];

    const content_id = await generateContentId({ conn: muzConn, board_id });
    const app_id = await fetchLastAppId({ artist });

    await muzConn.query(
      `INSERT INTO  ${targetContentsTable}(app_id, group_id, user_id, content_id, board_id, content_desc, idate, href)
                            VALUES (?, ?, ?, ?, ?, ?, ?, ?)`,
      [app_id, artist_nick.app_grp, artist_nick.seq, content_id, board_id, post.description, post.posted_date, post.url_id],
    );

    for (const media of mediaList) {
      const seq = await generateMediaId({ conn: muzConn });
      await muzConn.query(
        `INSERT INTO ${targetMediaTable}(seq, artist_seq, content_id, src)
                            VALUES (?, ?, ?, ?)`,
        [seq, artist_nick.seq, content_id, media.src],
      );
    }
  } catch (e) {
    console.log(e);
  }
};

(async () => {
  const conn = await pool.getConnection();
  const muzConn = await muzPool.getConnection();

  const [postList] = await conn.query(`SELECT * FROM SNS_POST ORDER BY id desc;`);

  for (const [index, post] of Object.entries(postList)) {
    await exportPost({ post, conn, muzConn });
    process.stdout.write(`${index} / ${postList.length} ... \r`);
  }

  process.exit(0);
})();
