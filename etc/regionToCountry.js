const puppeteer = require('puppeteer');
const fs = require('fs');
const path = require('path');

const waitor = {
  waitUntil: 'networkidle0',
};

const google_search_url = 'https://www.google.com/search?q=';
const set = new Set();

const countriesJSON = JSON.parse(fs.readFileSync(path.join(__dirname, './countries.json')));
const countries = countriesJSON.map((v) => { return v.label });
const regionsJSON = JSON.parse(fs.readFileSync(path.join(__dirname, './region.json')));

const crawlAllCountryInfo = async () => {
    
    regionsJSON.forEach((v) => {
        set.add(v.country);
    }) 

    Array.from(set).forEach((v) => {
        if(!countries.includes(v)){
            console.log(v);
        }
    })
};

(async () => {
    await crawlAllCountryInfo();
  console.log('전부 끝');
})();
