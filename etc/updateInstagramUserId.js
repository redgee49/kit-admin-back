const axios = require('axios').default;
const pool = require('../config/mySQL');

(async () => {
  const conn = await pool.getConnection();

  const [artistList] = await conn.query(
    `SELECT * FROM SNS_ARTIST 
          WHERE sns_enum = 'instagram' AND auth_id is null
        `,
  );

  for (const artist of artistList) {
    const userURL = `https://www.instagram.com/${encodeURI(artist.user_id)}/?__a=1`;
    try {
      const auth = await axios.get(userURL);
      await conn.query(
        `
                UPDATE SNS_ARTIST
                SET auth_id = ?
                WHERE id = ?
            `,
        [auth.data.graphql.user.id, artist.id],
      );
    } catch (e) {
      console.log(e);
      continue;
    }
  }

  process.exit(0);
})();
