const puppeteer = require('puppeteer');
const fs = require('fs');
const path = require('path');

const waitor = {
  waitUntil: 'networkidle0',
};

const google_search_url = 'https://www.google.com/search?q=';

const citiesJSON = JSON.parse(fs.readFileSync(path.join(__dirname, './city_clone.json')));
const cities = citiesJSON.slice();

const data = [];

function delay(timeout){
    return new Promise((res) => {
        setTimeout(() => {
            res();
        }, timeout);
    })
}

const crawlAllCountryInfo = async () => {

    const browser = await puppeteer.launch({ headless : false });
    const page = await browser.newPage();
    await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36');

    for (const city of cities) {
        if(city.country !== 'not_found'){
            data.push(city);
        }else{
            await delay(2000);
            await page.goto(`${google_search_url}${city.name}'s country`, waitor);

            try {
                await page.waitForSelector('div[data-tts="answers"]', { timeout: 1000 });

                const temp = { ...city };

                const textContent = await page.evaluate(() => {
                    const { textContent } = document.querySelector('div[data-tts="answers"]');
                    return textContent;
                });

                temp.country = textContent;
                
                console.log(`${city.name}'s county : ${textContent}`);
                data.push(temp);

            } catch (e) {

                try{
                    const wikiCountryField = await page.evaluate(() => {
                        const { textContent } = document.querySelector(`span[class="dvDNH"]`);
                        return textContent;
                    });
        
                    if(wikiCountryField === 'Country: '){
                        const country = await page.evaluate(() => {
                            const { textContent } = document.querySelector(`span[class="gyWzne"]`);
                            return textContent;
                        });
                        console.log(`${city.name}'s county : ${country}`);
                        data.push({ ...city, country });
                    }
                }catch(err){
                    console.log(`${city.name} can't found`)
                    data.push({ ...city, country : 'not_found' });
                }
            }finally{
                console.log(`${data.length} / ${cities.length}`);
                fs.writeFileSync(path.join(__dirname, `./attached_city.json`), JSON.stringify(data, null, 4));
                continue;
            }
        }
    }
    fs.writeFileSync(path.join(__dirname, `./attached_city.json`), JSON.stringify(data, null, 4));
};

(async () => {
    await crawlAllCountryInfo();
  console.log('전부 끝');
})();
