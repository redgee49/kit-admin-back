const puppeteer = require('puppeteer');

const waitor = {
  waitUntil: 'networkidle0',
};

const getTimeLine = async function ({ page }) {
  await page.waitForSelector('div[style*="flex-direction"] div > a');

  const timeline = await page.evaluate(async () => {
    let scrollHeight = 0;
    const results = [];

    const fetch = async () => {
      const delay = timeout => {
        return new Promise(res => {
          setTimeout(res, timeout);
        });
      };

      document.querySelectorAll('div[style*="flex-direction"] div > a').forEach(el => {
        const temp = {};

        temp.url = `https://www.instagram.com${el.getAttribute('href')}`;
        temp.thumbnail = el.querySelector('img').getAttribute('src');

        results.push(temp);
      });
      scrollHeight = document.body.scrollHeight;
      console.log(`갱신된 scroll Height : ${scrollHeight}`);
      // eslint-disable-next-line
			scrollTo(0, document.body.scrollHeight);

      await delay(3000);

      if (scrollHeight === document.body.scrollHeight) {
        console.log(`scrollHeight : ${scrollHeight}, body height : ${document.body.scrollHeight}`);
      } else {
        await fetch();
      }
    };

    await fetch();

    return results;
  });

  return timeline;
};

(async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();

  // Wait until page has loaded

  await page.goto('https://www.instagram.com/accounts/login/', waitor);

  // Wait for log in form

  await Promise.all([page.waitForSelector('[name="username"]'), page.waitForSelector('[name="password"]')]);

  // Enter username and password

  await page.type('[name="username"]', 'h0ng.jh');
  await page.type('[name="password"]', 'ghdgns1256#');

  await Promise.all([page.click('[type="submit"]'), page.waitForNavigation(waitor)]);

  await Promise.all([page.click('[type="button"]:nth-child(1)'), page.waitForNavigation(waitor)]);

  await page.goto('https://www.instagram.com/mirae.lee_pro/', waitor);

  const result = await getTimeLine({ page, row: 1, index: 1 });

  console.log(result);
})();
