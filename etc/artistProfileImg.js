const puppeteer = require('puppeteer');
const fs = require('fs');

const waitor = {
  waitUntil: 'networkidle0',
};

const daum_url = 'https://search.daum.net/search?w=tot&DA=YZR&t__nil_searchbox=btn&sug=&sugo=&sq=&o=&q=';
const artist_name = ['안예은',
'K.N CORPORATION, JAPAN',
'Amanda',
'아스트로',
'최유정',
'청하',
'혁(VIXX)',
'안녕하신가영',
'정세운',
'지수연',
'김동현(AB6IX)',
'김도연',
'크나큰',
'박세진 김윤주(옥상달빛)',
'라비',
'노태현',
'성진환',
'이성종, 남우현(인피니트)',
'윤도현',
'Young K',
'요조',
'오디오 시네마',
'BADBOSS CREW X Emilia Ali',
'백현',
'빅뱅',
'Black Desert Original Sound Selection',
'블랙핑크',
'첸',
'CJ ENM',
'드림캐쳐',
'K.N CORPORATION, JAPAN',
'엑소',
'세훈&찬열',
'FORESTELLA',
'프로미스나인',
'에프엑스',
'여자친구',
'소녀시대',
'구구단',
'공선옥',
'호우',
'IN2IT',
'IZ*ONE',
'종현',
'三代目 J Soul Brothers',
'박준호 & 유희경',
'KCON 2018',
'국헌&유빈',
'김성규',
'크나큰',
'규현',
'레오',
'러블리즈',
'2018 MAMA',
'마마무',
'최강창민',
'Michael Jackso',
'MINO',
'모모랜드',
'몬스타엑스',
'문별',
'DMuzTest1',
'MuzLive2',
'남우현',
'나띠',
'NCT',
'NCT127',
'NCT DREAM',
'뉴이스트',
'뉴이스트 W',
'K.N CORPORATION, JAPAN',
'박찬호',
'RatPac Documentary Films',
'라비',
'레드벨벳',
'구구단 세미나',
'설하윤',
'세븐틴',
'SF9',
'샤이니',
'키',
'신화',
'신비와 노래해요',
'JTBC DIGITAL STUDIO',
'수호',
'SUPER JUNIOR',
'슈퍼주니어-D&E',
'태민',
'태연',
'Taylor Swift',
'tvN',
'tvN 드라마',
'동방신기',
'유노윤호',
'Various Artists',
'베리베리',
'빅스',
'빅스 LR',
'The Secret',
'WangHong',
'워너원',
'위키미키',
'WOODZ(조승연)',
'X1',
'XIA(준수)',
'YESUNG',
'윤아']


const crawlAllArtistProfile = async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  const data = [];

  for (const artist of artist_name) {
    const temp = {};
  
    await page.goto(`${daum_url}${artist}`, waitor);

    try {
      await page.waitForSelector('img[data-dstype*="profile"]', { timeout: 2000 });

     const src = await page.evaluate(() => {
        const { src } = document.querySelector('img[data-dstype*="profile"]');
        return src;
    });

    temp.src = src;
    temp.name = artist;
    
      data.push(temp);
      fs.writeFileSync('./src.json', JSON.stringify(data, null, 4));
    } catch (e) {
      console.log(artist);
      continue;
    }
}
};

(async () => {
    await crawlAllArtistProfile();
  console.log('전부 끝');
})();
