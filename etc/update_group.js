const puppeteer = require('puppeteer');
const kit = require('../config/mySQL');

const NAVER_QUERY = 'https://search.naver.com/search.naver?sm=top_hty&fbm=1&ie=utf8&query=';
const waitor = {
  waitUntil: 'networkidle0',
};
const SNS = [
  {
    name: '페이스북',
    address: 'https://www.facebook.com/',
    en_name: 'facebook',
  },
  {
    name: '트위터',
    address: 'https://twitter.com/',
    en_name: 'twitter',
  },
  {
    name: '인스타그램',
    address: 'https://www.instagram.com/',
    en_name: 'instagram',
  },
];

const crawlAllArtistMembers = async sns_enum => {
  const conn = await kit.getConnection();

  const [artistList] = await conn.query(
    `SELECT * 
         FROM SNS_ARTIST
         WHERE id in (286, 74)
         GROUP BY group_name, artist_name
        `,
  );

  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();

  const data = [];
  const notExist = [];

  for (const artist of artistList) {
    const group_name = artist.artist_name;

    await page.goto(`${NAVER_QUERY}${group_name}`, waitor);

    try {
      await page.waitForSelector('.detail_profile > dd.name', { timeout: 3000 });
    } catch (e) {
      continue;
    }

    const memeberMenuExist = await page.evaluate(() => {
      const exist = Array.from(document.querySelectorAll('.detail_profile dt')).find(el => {
        return el.textContent.includes('멤버');
      });

      return Boolean(exist);
    });

    if (!memeberMenuExist) {
      continue;
    }

    console.log(`${group_name} 맴버들 수집 시작`);

    const members = await page.evaluate(() => {
      return Array.from(document.querySelectorAll('.detail_profile dd:nth-of-type(2) > a')).map(v => {
        return v.textContent;
      });
    });

    const targetSNS = SNS.find(v => v.name === sns_enum);

    const url = await page.evaluate(
      // eslint-disable-next-line
			({ targetSNS  }) => {
        const el = Array.from(document.querySelectorAll('.detail_profile > dd > a')).find(element => {
          return element.textContent.includes(targetSNS.name);
        });

        return el && el.href;
      },
      { targetSNS },
    );

    for (const member of members) {
      await page.goto(`${NAVER_QUERY}${group_name} ${member}`, waitor);

      try {
        await page.waitForSelector('.detail_profile > dd > a', { timeout: 2000 });

        const member_url = await page.evaluate(
          // eslint-disable-next-line
					({ targetSNS }) => {
            const { href } = Array.from(document.querySelectorAll('.detail_profile > dd > a')).find(el => {
              return el.textContent.includes(targetSNS.name);
            });

            return href;
          },
          { targetSNS },
        );

        const temp = {};

        if (member_url === url) {
          console.log(`group url : ${url} equals member url : ${url}`);
          continue;
        }

        temp.artist = `${group_name} ${member}`.replace(/'/g, "\\'");
        temp.artist_name = `${group_name} ${member}`.replace(/'/g, "\\'");
        temp.group_name = artist.group_name;
        temp.user_id = member_url.replace(targetSNS.address, '');
        temp.sns_enum = targetSNS.en_name;

        data.push(temp);
      } catch (e) {
        notExist.push(member);
        console.log(member);
        continue;
      }
    }
  }

  console.log('끝');

  for (const el of data) {
    try {
      const [exist] = await conn.query(
        `
                SELECT id 
                FROM SNS_ARTIST 
                WHERE artist = ? AND artist_name = ? AND group_name = ?
                      AND user_id = ? AND sns_enum = ?
        `,
        [el.artist, el.artist_name, el.group_name, el.user_id, el.sns_enum],
      );

      if (exist.length < 1) {
        await conn.query(
          `
                INSERT INTO SNS_ARTIST(artist, artist_name, group_name, user_id, sns_enum)
                VALUES ( ?, ?, ?, ?, ? )
            `,
          [el.artist, el.artist_name, el.group_name, el.user_id, el.sns_enum],
        );
      }
    } catch (e) {
      console.log(e);
      continue;
    }
  }

  console.log(notExist);
};

(async () => {
  await Promise.all(
    ['인스타그램', '트위터', '페이스북'].map(async v => {
      await crawlAllArtistMembers(v);
    }),
  );
  console.log('전부 끝');
})();
