const moment = require('moment');
const twitter = require('./twitter');
const pool = require('../config/muzSQL');

const { findAllArtist, findLastRow } = require('../query/CRAWL_QUERY');
const { insertPost } = require('../utils/insertSNS');

(async () => {
  const conn = await pool.getConnection();

  const [artistList] = await conn.query(findAllArtist, ['twitter']);

  console.log(`${moment().format('YYYY-MM-DD HH:mm:ss')} twitter crawling 시작`);

  for (const artist of artistList) {
    try {
      const [lastRow] = await conn.query(findLastRow, [artist.seq]);

      const crawledData = await twitter.crawl({
        user_id: artist.sns_id,
        since_id: lastRow.length && lastRow[0].href,
        idate: lastRow.length && lastRow[0].idate,
      });

      for (const row of crawledData) {
        await insertPost({ post: row, artist });
      }
      crawledData.length && console.log(`${artist.artist_nick_kr} ${crawledData.length}건 완료`);
    } catch (e) {
      console.log(e);
      continue;
    }
  }
  console.log(`크롤링 ${moment().format('YYYY-MM-DD HH:mm:ss')} 완료 \n`);

  process.exit(0);
})();
