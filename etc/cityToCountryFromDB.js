const puppeteer = require('puppeteer');
const fs = require('fs');
const path = require('path');

const waitor = {
  waitUntil: 'networkidle0',
};

const google_search_url = 'https://www.google.com/search?q=';

const cities = JSON.parse(fs.readFileSync(path.join(__dirname, './city_clone.json')));


const data = JSON.parse(fs.readFileSync(path.join(__dirname, './attached_city.json')));

const crawlAllCountryInfo = async () => {
  const c = cities.map((v) => {
    if(v.country === 'not_found'){
      const exist = data.find((d) =>  d.name === v.name);
      if(exist){
        v.country = exist.country;
      }else{
        v.country = 'not_found';
      }
    }

    return v;
  })

  fs.writeFileSync(path.join(__dirname, './city_clone.json'), JSON.stringify(c, null, 4));
};

(async () => {
    await crawlAllCountryInfo();
  console.log('전부 끝');
})();
