const muzPool = require('../config/muzSQL');
const kihnoPool = require('../config/kihno');

const moment = require('moment');
const {
  findBoardId,
  existPost,
  insertContent,
  existMedia,
  insertMedia,
  findMinContentId,
  findMinSeq,
  findOldestAppId,
  checkProfileURL,
  updateArtistProfile
} = require('../query/INSERT_SNS_QUERY');
const { fileSave, s3Upload } = require('../utils');
const slack = require('../utils/instaSlack');

const INT_MAX_VALUE = 2147483647;

async function generateContentId({ conn, board_id }) {
  const [list] = await conn.query(findMinContentId, [board_id]);
  if (list[0].content_id === null) {
    return INT_MAX_VALUE;
  }

  return list[0].content_id - 1;
}

async function generateMediaId({ conn }) {
  const [list] = await conn.query(findMinSeq);
  if (list[0].seq === null) {
    return INT_MAX_VALUE;
  }

  return list[0].seq - 1;
}

async function fetchLastAppId({ artist }) {
  const conn = await kihnoPool.getConnection();
  try {
    const [res] = await conn.query(findOldestAppId, [artist.app_grp]);
    return res[0].app_id;
  } catch (e) {
    console.log(e);
  } finally {
    conn.release();
  }
};


async function s3InsertVideo({ seq, conn, artist, content_id, media, post }){
 try{
  await fileSave({url : media});

  const path = `starTab/Video/${moment(post.idate).format('YYYY-MM-DD')}/${artist.app_grp}/${artist.seq}`;
  const fileName = media.match(/[^/s]+mp4/g).shift();

  await s3Upload({
    dir : path,
    fileName,
  })

  const videos = post.mediaList.filter((v) => v.includes('.mp4'));
  const index = videos.findIndex((v) => v.includes(media));

  const thumbnail = post.thumbnail.length && post.thumbnail[index];
  await fileSave({url : thumbnail, exec : 'jpg' });

  const thumbnail_path = `starTab/Thumbnail/${moment(post.idate).format('YYYY-MM-DD')}/${artist.app_grp}/${artist.seq}`;
  const thumbnail_fileName = thumbnail.match(/[^/s]+png|[^/s]+jpg|[^/s]+jpeg/g).shift();

  await s3Upload({
    dir : thumbnail_path,
    fileName : thumbnail_fileName,
    exec : 'jpg'
  })

  await conn.query(insertMedia, [seq, artist.seq, content_id, `${path}/${fileName}`, `${thumbnail_path}/${thumbnail_fileName}`])
 }catch(err){
   console.log(err);
   await slack.error({ title : 's3InsertVideo() 에러', text: err.message });
 }
}

async function s3InsertPhoto({ seq, conn, artist, content_id, media, post }){
  try{
    await fileSave({url : media, exec : 'jpg'});

    const path = `starTab/Photos/${moment(post.idate).format('YYYY-MM-DD')}/${artist.app_grp}/${artist.seq}`;
    const fileName = media.match(/[^/s]+png|[^/s]+jpg|[^/s]+jpeg/g).shift();

    await s3Upload({
      dir : path,
      fileName,
      exec : 'jpg'
    })


    await conn.query(insertMedia, [seq, artist.seq, content_id, `${path}/${fileName}`, null])
  }catch(err){
    console.log(err);
    await slack.error({ title: 's3InsertPhoto()', text: err.message });
  }
}

async function insertVideo({ seq, conn, artist, content_id, media, post }){
  const videos = post.mediaList.filter((v) => v.includes('mp4'));
  const index = videos.findIndex((v) => v.includes(media));

  const thumbnail = post.thumbnail.length && post.thumbnail[index];

  await conn.query(insertMedia, [seq, artist.seq, content_id, media, thumbnail]);
}

async function insertMedias({ conn, artist, content_id, media, post }) {
  const seq = await generateMediaId({ conn });
  const [mediaExist] = await conn.query(existMedia, [seq, artist.seq, content_id, media]);
  if (mediaExist.length === 0) {
    if(artist.sns_enum === 'instagram' && media.includes('.mp4')){
      await s3InsertVideo({ seq, conn, artist, content_id, media, post });
    }else if (artist.sns_enum === 'instagram'){
      await s3InsertPhoto({ seq, conn, artist, content_id, media, post });
    }else if(media.includes('.mp4')){
      await insertVideo({ seq, conn, artist, content_id, media, post });
    }else{
      await conn.query(insertMedia, [seq, artist.seq, content_id, media, null]);
    }
  }else{
    console.log(`${media} 중복`);
  }
}

async function updateProfileImage({ seq, sns_enum, url }){
  const conn = await muzPool.getConnection();
  try{
    const TO_DAY = moment().format('YYYY-MM-DD');
    const [result] = await conn.query(checkProfileURL, [seq]);
    const fileName = sns_enum === 'youtube' ? [url.replace('https://yt3.ggpht.com/', '').replace('/', '').slice(0, 20) + '.jpg'] : url.match(/[^/s]+png|[^/s]+jpg|[^/s]+jpeg/g);
    if(result[0].photo_file_url !== null || String(result[0].photo_file_url).includes(fileName) || fileName.includes(String(result[0].photo_file_url))){
      // DB 내에 존재하는 프로필 사진과 같은 사진임
    }else{
      console.log(`detected not same profile url. save current profile status...`);
      await fileSave({ url, exec : 'jpg' });
      const profile_image_path = `starTab/Profile_Image/${sns_enum.toUpperCase()}/${result[0].app_grp.replace(/[\{\}\[\]\/?.,;:|\)*~`!^\-+<>@\#$%&\\\=\(\'\"]/gi, '')}/${TO_DAY}`;
      const profile_fileName = fileName[0];

      await s3Upload({
        dir : profile_image_path,
        fileName : profile_fileName,
        exec : 'jpg'
      })

      await conn.query(updateArtistProfile, [`${profile_image_path}/${profile_fileName}`, TO_DAY, seq]);
    }
  }catch(err) {
    console.log(err)
    await slack.error({ title: 'updateProfile() 에러', text: err.message });
  }finally{
    conn.release();
  }
}

async function insertPost({ artist, post, sns_enum }) {
  const conn = await muzPool.getConnection();
  try {
    const [board_info] = await conn.query(findBoardId, [artist.app_grp, 'star']);

    const board_id = board_info[0].old_board_id;
    if(!board_id || !board_info.length) {
      await slack.error({ title: `insertPost() 에러 -${artist.app_grp}`, text: 'old_board_id 를 찾을 수 없습니다.' })
      throw new Error('Can not found old_board_id');
    }

    const content_id = await generateContentId({ conn, board_id });
    const app_id = await fetchLastAppId({ artist });

    const [exist] = await conn.query(existPost, [
      artist.seq || null,
      post.href || null,
    ]);

    if (exist.length === 0) {
      console.log(`게시물 인서트 : app_id : ${app_id}, content_id : ${content_id}, board_id : ${board_id} artist_seq : ${artist.seq}, idate : ${post.idate}`);
      post.description && console.log(`description : ${post.description}\n`);
      await conn.query(insertContent, [
        app_id,
        artist.app_grp,
        artist.seq, 
        content_id,
        board_id,
        (post.description && post.description.replace(/'/g, "\\'")) || null,
        post.idate,
        post.href,
        post.meta,
      ]);
      // SNS 게시물 인서트

      if (post.mediaList?.length) {
        const { mediaList } = post;

        for (const media of mediaList) {
          await insertMedias({ conn, artist, content_id, media, post });
        }
      }
    }
  } catch (e) {
    console.log(e);
  } finally {
    conn.release();
  }
};

module.exports = {
  insertPost,
  updateProfileImage
};
