const puppeteer = require('puppeteer-extra');
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
puppeteer.use(StealthPlugin());

exports.launchBrowser = async() => {
    try{
        const browser = await puppeteer.launch({
            headless : process.env.NODE_ENV === 'production',
            args : [
                '--no-sandbox'
                // `--proxy-server=${proxyList[0].ip}`,
                // '--ignore-certificate-errors',
                // '--ignore-certificate-errors-spki-list '
            ]
        })
        const page = await browser.newPage(); 
        await page.setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36");
        await page.setExtraHTTPHeaders({
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36',
            'upgrade-insecure-requests': '1',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9,en;q=0.8',
            'X-Instagram-AJAX': '1',
            'X-Requested-With': 'XMLHttpRequest'
          })
        return [browser, page];
    }catch(err){
        console.error(err);
        throw err;
    }
}