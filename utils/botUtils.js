
exports.arrangeText = (text) => {
    const array = text.split('\n');
    const temp = {};
    array.forEach(v => { 
        const key = v.split(':')[0];
        const value = v.split(':')[1];
        temp[key] = value;
    })
    return temp;
}