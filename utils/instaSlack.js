const { default : axios } = require('axios');
const { log } = require('.');

const BASE_URL = 'https://hooks.slack.com/services/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';

const infoColor = "#36a64f";
const warnColor = "#FFCC00";
const errorColor = "#cc3300";


exports.info = async({ title, text }) => {
    log(`${title} - ${text}`);
    await axios.post(BASE_URL, createInstaAttachment({ color : infoColor, title, text }));
}

exports.warn = async({ title, text }) => {
    log(`${title} - ${text}`);
    await axios.post(BASE_URL, createInstaAttachment({ color : warnColor, title, text }));
}

exports.error = async({ title, text }) => {
    log(`${title} - ${text}`);
    await axios.post(BASE_URL, createInstaAttachment({ color : errorColor, title, text }));
}


const createInstaAttachment = ({ color = infoColor, title, text }) => {
    return {
        "attachments": [
            {
                "color": color,
                "pretext": "info",
                "author_name": "instagram crawler",
                "title": title,
                "text": text,
                "footer": "instagram crawler",
                "footer_icon": "https://platform.slack-edge.com/img/default_application_icon.png",
            }
        ]
    }
}
