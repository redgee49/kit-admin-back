const fs = require('fs');	
const path = require('path');	
const request = require('request');
const moment = require('moment');
const AWS = require('aws-sdk');
const s3 = new AWS.S3({ accessKeyId : process.env.S3_ACCESS_KEY_ID, secretAccessKey : process.env.S3_SECRET_KEY });

const BUCKENT_NAME = 'sns-seoul.muzlive.com';

const getResponse = url => {
  url = url && url.replace('https', 'http');
  return new Promise((res, rej) => {
    request(
      {
        url: url,
        encoding: null,
      },
      (err, response, body) => {
        if(err){
          console.log(url);
          rej(err);
        }
        res({ response, body });
      },
    ); 
  });
};

const fileSave = async({ url, exec = 'mp4' }) => {
  try{
    const buffer = await getResponse(url);
    fs.writeFileSync(path.join(__dirname, `../temp/temporary.${exec}`), buffer.body);
    const stats = fs.statSync(path.join(__dirname, `../temp/temporary.${exec}`));
    if(stats.size / 1000000.0 < 0.001){
      // 파일사이즈가 1kb 보다 작다면 경고
      console.log(`too small size ${url}`);
    }
    
  }catch(err){
    console.log(err);
  }
}

const s3Upload = ({ dir, fileName, exec = 'mp4' }) => {
  const fileContent = fs.readFileSync(path.join(__dirname, `../temp/temporary.${exec}`));

  const params = {
    Bucket : `${BUCKENT_NAME}/${dir}`,
    Key : fileName,
    Body : fileContent
  }

  return new Promise((res, rej) => {
    s3.upload( params, (err, data) => {
      if(err)
        rej(err);
      res(data);
    })
  })
}

const log = (message) => {
  console.log(`${moment().format('YYYY-MM-DD HH:mm')} ${message}`);
};


/**
 * 
 * @param {*} ms 시간 (ms) default : 3초
 * @returns 입력한 시간에 10초 이하의 ms 값을 더한 시간만큼 delay시킴
 */
const delay = (ms = 1000 * 3) => {
  return new Promise((res) => {
    setTimeout(() => {
      res();
    }, ms + Math.random() * 1000 * 10);
  })
}


module.exports = {
  delay,
  fileSave,
  s3Upload,
  s3,
  BUCKENT_NAME,
  log
};
