const { default : axios } = require('axios');
const { log } = require('.');

const BASE_URL = 'https://hooks.slack.com/services/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';

const infoColor = "#36a64f";
const warnColor = "#FFCC00";
const errorColor = "#cc3300";


exports.info = async({ title, text }) => {
    log(text);
    await axios.post(BASE_URL, createMicAttachment({ color : infoColor, title, text }));
}

exports.warn = async({ title, text }) => {
    log(text);
    await axios.post(BASE_URL, createMicAttachment({ color : warnColor, title, text }));
}

exports.error = async({ title, text }) => {
    log(text);
    await axios.post(BASE_URL, createMicAttachment({ color : errorColor, title, text }));
}



const createMicAttachment = ({ color = infoColor, title, text }) => {
    return {
        "attachments": [
            {
                "color": color,
                "pretext": "info",
                "author_name": "daily mic status bot :)",
                "title": title,
                "text": text,
                "footer": "daily mic status bot :)",
                "footer_icon": "https://platform.slack-edge.com/img/default_application_icon.png",
            }
        ]
    }
}