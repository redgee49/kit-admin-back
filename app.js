const express = require('express');
const path = require('path');

const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');

const cookieParser = require('cookie-parser');
const helmet = require('helmet');
const passport = require('passport');
const session = require('express-session');
const redis = require('redis');
const ConnectRedis = require('connect-redis')(session);

const client = redis.createClient(6379, 'localhost');

const flash = require('connect-flash');
require('./config/passport')(passport);

const PORT = process.env.PORT || 8080;

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  }),
);
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
      secure: false,
      maxAge: 1000 * 60 * 60 * 24 * 30,
      httpOnly: false,
      domain: process.env.NODE_ENV === 'production' ? 'admin.kitinside.com' : 'localhost',
    }, // 30 days
    store: new ConnectRedis({
      client,
      ttl: 200
    })
  }),
);
app.use(
  cors({
    origin: ['http://localhost:3000', /\admin.kitinside.com$/],
    credentials: true,
  }),
);

app.use(helmet());
app.use(cookieParser());
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use('/api/album', require('./routes/album-Router.js'));
app.use('/api/board', require('./routes/board-Router.js'));
app.use('/api/artist', require('./routes/artist-Router.js'));
app.use('/api/company', require('./routes/company-Router.js'));
app.use('/api/sns', require('./routes/sns-Router.js'));
app.use('/api/test', require('./routes/test-Router.js'));
app.use('/api/user', require('./routes/user-Router.js'));
app.use('/api/s3', require('./routes/s3-Router.js'));
app.use('/api/device', require('./routes/device-Router.js'));

app.use('/', express.static(path.join(__dirname, './uploads')));

// if (process.env.NODE_ENV === 'production') {
//   app.use(express.static(path.join(__dirname, './client/build')));
//   app.get('*', (req, res) => {
//     res.sendFile(path.resolve(__dirname, './client/build', 'index.html'));
//   });
// }

app.listen(PORT, () => {
  console.log(`Successfully started on PORT ${PORT}`);
});
