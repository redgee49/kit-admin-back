const targetArtistTable = 'sns_artist_nick';
const targetMediaTable = 'sns_media_info';
const targetContentsTable = 'sns_contents_info';

module.exports = {
  findBoardId: `
        SELECT * 
        FROM board 
        WHERE group_id = ? AND title = ?
        `,

  existPost: `
        SELECT *
        FROM ${targetContentsTable}
        WHERE 
            user_id = ? AND media_id = ?
        `,

  insertContent: `INSERT INTO  ${targetContentsTable}(app_id, group_id, user_id, content_id, board_id, content_desc, idate, media_id, meta )
         VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)`,

  existMedia: `SELECT * FROM ${targetMediaTable} WHERE seq = ? AND artist_seq = ? AND content_id = ? AND src = ?`,

  insertMedia: `INSERT INTO ${targetMediaTable}(seq, artist_seq, content_id, src, thumbnail)
         VALUES (?, ?, ?, ?, ?)`,

  findMinContentId: `SELECT MIN(content_id) as content_id FROM ${targetContentsTable} WHERE board_id = ?`,

  findMinSeq: `SELECT MIN(seq) as seq FROM ${targetMediaTable}`,

  findOldestAppId: `SELECT app_id FROM kihno_app_info WHERE app_grp = ? ORDER BY idate;`,

  updateThumbnail : `UPDATE ${targetContentsTable} SET thumbnail = ?  WHERE app_id = ? AND content_id = ? AND board_id = ?`,

  checkProfileURL : `SELECT app_grp, artist_nick_kr, photo_file_url FROM ${targetArtistTable} WHERE seq = ?`,

  updateArtistProfile : `UPDATE ${targetArtistTable} SET photo_file_url = ?, idate = ?  WHERE seq = ?`,
};
