module.exports = {
  getTimeLine: `SELECT 
            p.url_id, p.thumbnail, p.description, c.post_id, c.src
        FROM
            SNS_ARTIST a,
            SNS_POST p,
            SNS_CONTENTS c
        WHERE
            a.artist_name = ?
                AND a.sns_enum = ?
                AND a.id = p.artist_id
                AND p.id = c.post_id;
        `,

  wholeTimeLineBySNS: 
        `SELECT 
            a.artist_nick_kr,
            a.sns_id,
            a.sns_enum,
            p.href,
            c.thumbnail,
            p.content_desc,
            c.seq,
            c.content_id,
            c.src,
            p.idate
        FROM
            SNS_contents_info p LEFT JOIN SNS_artist_nick a ON a.seq = p.user_id
            LEFT JOIN SNS_media_info c ON p.content_id = c.content_id and a.seq = c.artist_seq
        WHERE
            a.sns_enum = ? and p.idate < ?
        GROUP BY p.content_id, c.src
        ORDER BY p.idate DESC
        LIMIT 0, 500;
        `,
    wholeArtistList: 
        `SELECT
            a.artist_nick_kr,
            a.sns_id,
            a.sns_enum,
            p.href,
            c.thumbnail,
            p.content_desc,
            c.seq,
            c.content_id,
            c.src,
            p.idate
        FROM
            SNS_contents_info p LEFT JOIN SNS_artist_nick a
                ON a.seq = p.user_id
            LEFT JOIN SNS_media_info c ON p.content_id = c.content_id and a.seq = c.artist_seq
        WHERE
            a.sns_enum = ?
        GROUP BY a.sns_id
        ORDER BY p.idate DESC
        LIMIT ?, ?;
    `,
    recent: 
    `
    select c.content_desc as 'description', IFNULL(m.thumbnail, m.src) as 'image'
    from SNS_contents_info c LEFT OUTER JOIN SNS_media_info m ON c.content_id = m.content_id AND c.user_id = m.artist_seq
    where c.group_id = ? AND m.src not like 'star%'
    group by c.content_id
    LIMIT 0, 4
    `
};
