const artistTalbe = `sns_artist_nick`;
const contentsTable = `sns_contents_info`;

module.exports = {
  findAllArtist: `SELECT * FROM ${artistTalbe} 
         WHERE sns_enum = ?
         ORDER BY seq;
        `,

findOneArtist: `SELECT * FROM ${artistTalbe} 
        WHERE sns_enum = ? and seq = ?
        ORDER BY seq;
       `,

  findLastRow: `SELECT * FROM ${contentsTable}
         WHERE user_id = ?
         ORDER BY idate desc
         limit 0, 1;
        `,
};
