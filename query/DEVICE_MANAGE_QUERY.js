exports.invalidRankList = ({ startDate, endDate }) => {
    return `
    SELECT
    C.model_name, C.model_id, C.count, C.input_YN
    FROM (
            (SELECT
            CASE
            WHEN C.model_id IS NOT NULL THEN 'Y'
            ELSE 'N'
            END AS input_YN, A.model_name, C.model_id, SUM(A.count) AS count
            FROM
                (SELECT input_YN, model_name, COUNT(*) AS count
                    FROM device_mike_earphone_log F
                    WHERE NOT (model_name LIKE ('iPhone%') OR model_name LIKE ('iPad%'))
                    AND F.idate >= '${startDate}' AND F.idate <= '${endDate}'
                    GROUP BY model_name) A
            LEFT JOIN device_model_name_info B ON A.model_name = B.model_name
            LEFT JOIN device_mike_earphone_info C ON B.marketing_name = C.model_id
            GROUP BY A.model_name)
        UNION ALL
            (SELECT
            CASE
                WHEN B.model_id IS NOT NULL THEN 'Y'
                ELSE 'N'
                END AS input_YN,
                A.model_name, A.model_name AS model_id, COUNT(*) AS Count
            FROM device_mike_earphone_log A
            LEFT JOIN device_mike_earphone_info B ON B.model_id = A.model_name
            WHERE (A.model_name LIKE ('iPhone%') OR A.model_name LIKE ('iPad%'))
                    AND A.idate >= '${startDate}' AND A.idate <= '${endDate}'
            GROUP BY A.model_name
            )
        ) C
    WHERE input_YN = 'N' AND count >= 100
    ORDER BY C.count DESC;
`
}

exports.invalidList = ({ startDate, endDate }) => {
    return `
    SELECT
    C.model_name, C.model_id, C.count, C.input_YN
    FROM (
            (SELECT
            CASE
            WHEN C.model_id IS NOT NULL THEN 'Y'
            ELSE 'N'
            END AS input_YN, A.model_name, C.model_id, SUM(A.count) AS count
            FROM
                (SELECT input_YN, model_name, COUNT(*) AS count
                    FROM device_mike_earphone_log F
                    WHERE NOT (model_name LIKE ('iPhone%') OR model_name LIKE ('iPad%'))
                    AND F.idate >= '${startDate}' AND F.idate <= '${endDate}'
                    GROUP BY model_name) A
            LEFT JOIN device_model_name_info B ON A.model_name = B.model_name
            LEFT JOIN device_mike_earphone_info C ON B.marketing_name = C.model_id
            GROUP BY A.model_name)
        UNION ALL
            (SELECT
            CASE
                WHEN B.model_id IS NOT NULL THEN 'Y'
                ELSE 'N'
                END AS input_YN,
                A.model_name, A.model_name AS model_id, COUNT(*) AS Count
            FROM device_mike_earphone_log A
            LEFT JOIN device_mike_earphone_info B ON B.model_id = A.model_name
            WHERE (A.model_name LIKE ('iPhone%') OR A.model_name LIKE ('iPad%'))
                    AND A.idate >= '${startDate}' AND A.idate <= '${endDate}'
            GROUP BY A.model_name
            )
        ) C
    WHERE input_YN = 'N'
    ORDER BY C.count DESC;
`
}

exports.statusAndroid = ({ startDate, endDate }) => {
    return `
    SELECT
    SUM(CASE input_YN WHEN 'Y' THEN count else 0 end) / SUM(count) * 100 AS TOTAL_Y_PERCENT,
    SUM(CASE input_YN WHEN 'N' THEN count else 0 end) / SUM(count) * 100 AS TOTAL_N_PERCENT
    FROM (
        (SELECT
            CASE
            WHEN C.model_id IS NOT NULL THEN 'Y'
            ELSE 'N'
            END AS input_YN, A.model_name, C.model_id, SUM(A.count) AS count
            FROM
                (SELECT input_YN, model_name, COUNT(*) AS count
                    FROM device_mike_earphone_log F
                    WHERE NOT (model_name LIKE ('iPhone%') OR model_name LIKE ('iPad%'))
                    AND F.idate >= '${startDate}' AND F.idate <= '${endDate}'
                    GROUP BY model_name) A
            INNER JOIN device_model_name_info B ON A.model_name = B.model_name
            LEFT JOIN device_mike_earphone_info C ON B.marketing_name = C.model_id
            GROUP BY B.model_name
        )
    ) D
`
}

exports.statusApple = ({ startDate, endDate }) => {
    return `
    SELECT
    SUM(CASE input_YN WHEN 'Y' THEN count else 0 end) / SUM(count) * 100 AS TOTAL_Y_PERCENT,
    SUM(CASE input_YN WHEN 'N' THEN count else 0 end) / SUM(count) * 100 AS TOTAL_N_PERCENT
    FROM (
        SELECT
        CASE
            WHEN B.model_id IS NOT NULL THEN 'Y'
            ELSE 'N'
            END AS input_YN,
            A.model_name, A.model_name AS model_id, COUNT(*) AS Count
        FROM device_mike_earphone_log A
        LEFT JOIN device_mike_earphone_info B ON B.model_id = A.model_name
        WHERE (A.model_name LIKE ('iPhone%') OR A.model_name LIKE ('iPad%'))
                AND A.idate >= '${startDate}' AND A.idate <= '${endDate}'
        GROUP BY A.model_name
    ) D
`
}

exports.statusAll = ({ startDate, endDate }) => {
    return `
    SELECT
    SUM(CASE input_YN WHEN 'Y' THEN count else 0 end) / SUM(count) * 100 AS TOTAL_Y_PERCENT,
    SUM(CASE input_YN WHEN 'N' THEN count else 0 end) / SUM(count) * 100 AS TOTAL_N_PERCENT
    FROM (
        (SELECT
            CASE
            WHEN C.model_id IS NOT NULL THEN 'Y'
            ELSE 'N'
            END AS input_YN, A.model_name, C.model_id, SUM(A.count) AS count
            FROM
                (SELECT input_YN, model_name, COUNT(*) AS count
                    FROM device_mike_earphone_log F
                    WHERE NOT (model_name LIKE ('iPhone%') OR model_name LIKE ('iPad%'))
                    AND F.idate >= '${startDate}' AND F.idate <= '${endDate}'
                    GROUP BY model_name) A
            INNER JOIN device_model_name_info B ON A.model_name = B.model_name
            LEFT JOIN device_mike_earphone_info C ON B.marketing_name = C.model_id
            GROUP BY B.model_name
        )
        UNION ALL
        (SELECT
        CASE
            WHEN B.model_id IS NOT NULL THEN 'Y'
            ELSE 'N'
            END AS input_YN,
            A.model_name, A.model_name AS model_id, COUNT(*) AS Count
        FROM device_mike_earphone_log A
        LEFT JOIN device_mike_earphone_info B ON B.model_id = A.model_name
        WHERE (A.model_name LIKE ('iPhone%') OR A.model_name LIKE ('iPad%'))
                AND A.idate >= '${startDate}' AND A.idate <= '${endDate}'
        GROUP BY A.model_name
        )
    ) D
`
}

exports.currentList = () => {
    return `
        SELECT *
        FROM Smc_Admin.device_mike_earphone_info;
    `
}

exports.createDeviceQuery = ({ model_id, mike_place, earphone_place, mike_face_screen_length, location_mike_top, location_mike_left, earphone_face_screen_length, location_earphone_top, location_earphone_left }) => {
    return `
        INSERT INTO device_mike_earphone_info ( model_id, mike_place, earphone_place, mike_face_screen_length, location_mike_top, location_mike_left, earphone_face_screen_length, location_earphone_top, location_earphone_left )
            VALUES('${model_id}', '${mike_place}', '${earphone_place}', '${mike_face_screen_length}', '${location_mike_top}', '${location_mike_left}', '${earphone_face_screen_length}', '${location_earphone_top}', '${location_earphone_left}')`
}