module.exports = {
  artistListGroupByGroupName: `
        SELECT app_id, app_grp, artist_nm_kr, img_albumCoverNum as cover, idate
        FROM kihno_app_info
        WHERE (app_grp, idate) in (
            SELECT app_grp, MAX(idate) FROM kihno_app_info GROUP BY app_grp
        )
        ORDER BY idate desc;
        `,

  allArtistByGroupName: `
            SELECT artist_nick_kr, sns_enum, sns_id
            FROM sns_artist_nick
            WHERE app_grp = ?
            ORDER BY artist_nick_kr
        `,

  addArtistToGroup: `
            INSERT INTO sns_artist_nick (artist_nick_kr, artist_nick_en, app_grp, sns_id, sns_enum)
            VALUES (?, ?, ?, ?, ?);
        `,
  instaAddArtistToGroup: `
            INSERT INTO sns_artist_nick (artist_nick_kr, artist_nick_en, app_grp, sns_id, sns_enum, auth_id)
            VALUES (?, ?, ?, ?, ?, ?);
    `,

   currentAlbumList: `
            SELECT app_id, app_grp, artist_nm, img_albumCoverNum as 'coverNum', idate, release_date, album_nm
            FROM kihno_app_info
            ORDER BY idate DESC;
   `,
   distinctGroupName: `
            select distinct(app_grp)
            from sns_artist_nick
   `,
   botInsertArtistQuery: `
        INSERT INTO sns_artist_nick (artist_nick_kr, artist_nick_en, app_grp, sns_id, sns_enum, auth_id)
        VALUES (?, ?, ?, ?, ?, ?); 
   `
};
