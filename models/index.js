const Sequelize = require('sequelize');
const env = process.env.NODE_ENV || 'development';
const config = require('../config/db')[env];

const db = {};

const sequelize = new Sequelize(config.database, config.username, config.password, config);

// basic
db.User = require('./user')(sequelize, Sequelize);
// db.album = require('./album')(sequelize, Sequelize); *unused model*
// db.company = require('./company')(sequelize, Sequelize); *unused model*
db.daily_app_connect_gender_ratio = require('./daily_app_connect_gender_ratio')(
  sequelize,
  Sequelize,
);
db.daily_kit_connect_gender_ratio = require('./daily_kit_connect_gender_ratio')(
  sequelize,
  Sequelize,
);
db.daily_report = require('./daily_report')(sequelize, Sequelize);
db.albumInfo = require('./albumInfo')(sequelize, Sequelize);
db.songInfo = require('./songInfo')(sequelize, Sequelize);
db.videoInfo = require('./videoInfo')(sequelize, Sequelize);

// lake
db.albumAge = require('./lake/albumAge')(sequelize, Sequelize);
db.albumDeviceLanguage = require('./lake/albumDeviceLanguage')(sequelize, Sequelize);
db.albumDeviceModel = require('./lake/albumDeviceModel')(sequelize, Sequelize);
db.albumGender = require('./lake/albumGender')(sequelize, Sequelize);
db.albumGeography = require('./lake/albumGeography')(sequelize, Sequelize);
db.albumUsageCnt = require('./lake/albumUsageCnt')(sequelize, Sequelize);
db.albumUsageTime = require('./lake/albumUsageTime')(sequelize, Sequelize);
db.coordinates = require('./lake/coordinates')(sequelize, Sequelize);
db.endMusicPlay = require('./lake/endMusicPlay')(sequelize, Sequelize);
db.endVideoPlay = require('./lake/endVideoPlay')(sequelize, Sequelize);
db.musicPlay = require('./lake/musicPlay')(sequelize, Sequelize);
db.startMusicPlay = require('./lake/startMusicPlay')(sequelize, Sequelize);
db.studioRecord = require('./lake/studioRecord')(sequelize, Sequelize);
db.totalProd = require('./lake/total_prod')(sequelize, Sequelize);

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
