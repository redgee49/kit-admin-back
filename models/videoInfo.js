module.exports = (sequelize, DataTypes) => {
  const videoInfo = sequelize.define(
    'video_info',
    {
      video_type: {
        type: DataTypes.TEXT,
      },
      video_id: {
        type: DataTypes.TEXT,
      },
      video_title: {
        type: DataTypes.TEXT,
      },
      app_id: {
        type: DataTypes.TEXT,
      },
      app_grp: {
        type: DataTypes.TEXT,
      },
      artist: {
        type: DataTypes.TEXT,
      },
      video_order: {
        type: DataTypes.BIGINT,
      },
      package_nm: {
        type: DataTypes.TEXT,
      },
      duration: {
        type: DataTypes.BIGINT,
      },
      idate: {
        type: DataTypes.DATE,
      },
      youtube_title: {
        type: DataTypes.TEXT,
      },
      youtube_order: {
        type: DataTypes.BIGINT,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
      timestamp: false,
    },
  );
  videoInfo.associate = db => {};
  return videoInfo;
};
