module.exports = (sequelize, DataTypes) => {
  const songInfo = sequelize.define(
    'song_info',
    {
      cp3_id: {
        type: DataTypes.TEXT,
      },
      app_id: {
        type: DataTypes.TEXT,
      },
      app_grp: {
        type: DataTypes.TEXT,
      },
      song_title: {
        type: DataTypes.TEXT,
      },
      song_artist: {
        type: DataTypes.TEXT,
      },
      song_order: {
        type: DataTypes.BIGINT,
      },
      song_playtime: {
        type: DataTypes.BIGINT,
      },
      idate: {
        type: DataTypes.DATE,
      },
      package_nm: {
        type: DataTypes.TEXT,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
      timestamp: false,
    },
  );
  songInfo.associate = db => {};
  return songInfo;
};
