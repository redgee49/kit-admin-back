module.exports = (sequelize, DataTypes) => {
  const tb_firebaseData = sequelize.define(
    'tb_firebaseData',
    {
      event_category: {
        type: DataTypes.STRING(100),
      },
      event_action: {
        type: DataTypes.STRING(100),
      },
      event_label: {
        type: DataTypes.STRING(100),
      },
      user_pseudo_id: {
        type: DataTypes.STRING(100),
      },
      geo_continent: {
        type: DataTypes.STRING(100),
      },
      geo_country: {
        type: DataTypes.STRING(100),
      },
      geo_region: {
        type: DataTypes.STRING(100),
      },
      geo_city: {
        type: DataTypes.STRING(100),
      },
      event_timestamp: {
        type: DataTypes.DATE,
      },
      mobile_device_info: {
        type: DataTypes.STRING(100),
      },
      device_language: {
        type: DataTypes.STRING(100),
      },
      create_timestamp: {
        type: DataTypes.DATE,
      },
      image_id: {
        type: DataTypes.STRING(45),
      },
      video_id: {
        type: DataTypes.STRING(45),
      },
      device_operating_system: {
        type: DataTypes.STRING(45),
      },
      remain_time: {
        type: DataTypes.INTEGER,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  tb_firebaseData.associate = db => {};
  return tb_firebaseData;
};
