module.exports = (sequelize, DataTypes) => {
  const daily_report = sequelize.define(
    'daily_report',
    {
      date: {
        type: DataTypes.DATE,
      },
      app_id: {
        type: DataTypes.TEXT,
      },
      kit_user_cnt: {
        type: DataTypes.DOUBLE,
      },
      sum_kit_conn_cnt: {
        type: DataTypes.DOUBLE,
      },
      avg_kit_conn_cnt: {
        type: DataTypes.DOUBLE,
      },
      multi_connect_user: {
        type: DataTypes.DOUBLE,
      },
      total_kit_using_time: {
        type: DataTypes.DOUBLE,
      },
      avg_kit_using_time: {
        type: DataTypes.DOUBLE,
      },
      ext_user_cnt: {
        type: DataTypes.DOUBLE,
      },
      ext_user_ratio: {
        type: DataTypes.DOUBLE,
      },
      sum_ext_player_using_time: {
        type: DataTypes.DOUBLE,
      },
      avg_ext_player_using_time: {
        type: DataTypes.DOUBLE,
      },
      activated_kit_cnt: {
        type: DataTypes.DOUBLE,
      },
      activated_device_cnt: {
        type: DataTypes.DOUBLE,
      },
      avg_device_per_kit: {
        type: DataTypes.DOUBLE,
      },
      multi_device_user: {
        type: DataTypes.DOUBLE,
      },
      music_play_cnt: {
        type: DataTypes.DOUBLE,
      },
      ext_play_user_cnt: {
        type: DataTypes.DOUBLE,
      },
      ext_play_cnt: {
        type: DataTypes.DOUBLE,
      },
      video_play_cnt: {
        type: DataTypes.DOUBLE,
      },
      video_play_user_cnt: {
        type: DataTypes.DOUBLE,
      },
      view_cnt: {
        type: DataTypes.DOUBLE,
      },
      view_user_cnt: {
        type: DataTypes.DOUBLE,
      },
      capture_cnt: {
        type: DataTypes.DOUBLE,
      },
      capture_user_cnt: {
        type: DataTypes.DOUBLE,
      },
      fav_cnt: {
        type: DataTypes.DOUBLE,
      },
      fav_user_cnt: {
        type: DataTypes.DOUBLE,
      },
      studio_user_cnt: {
        type: DataTypes.DOUBLE,
      },
      studio_use_cnt: {
        type: DataTypes.DOUBLE,
      },
      studio_total_remain_time: {
        type: DataTypes.DOUBLE,
      },
      studio_1per_remain_time: {
        type: DataTypes.DOUBLE,
      },
      comm_connect_cnt: {
        type: DataTypes.DOUBLE,
      },
      comm_user_cnt: {
        type: DataTypes.DOUBLE,
      },
      sum_comm_using_time: {
        type: DataTypes.DOUBLE,
      },
      avg_comm_using_time: {
        type: DataTypes.DOUBLE,
      },
      total_post_cnt: {
        type: DataTypes.DOUBLE,
      },
      total_post_user_cnt: {
        type: DataTypes.DOUBLE,
      },
      total_comment_cnt: {
        type: DataTypes.DOUBLE,
      },
      total_commnet_user_cnt: {
        type: DataTypes.DOUBLE,
      },
      total_like_cnt: {
        type: DataTypes.DOUBLE,
      },
      total_like_user_cnt: {
        type: DataTypes.DOUBLE,
      },
      post_written_cnt: {
        type: DataTypes.DOUBLE,
      },
      post_written_user_cnt: {
        type: DataTypes.DOUBLE,
      },
      post_comment_cnt: {
        type: DataTypes.DOUBLE,
      },
      post_comment_user_cnt: {
        type: DataTypes.DOUBLE,
      },
      post_like_cnt: {
        type: DataTypes.DOUBLE,
      },
      post_like_user_cnt: {
        type: DataTypes.DOUBLE,
      },
      photo_written_cnt: {
        type: DataTypes.DOUBLE,
      },
      photo_written_user_cnt: {
        type: DataTypes.DOUBLE,
      },
      photo_comment_cnt: {
        type: DataTypes.DOUBLE,
      },
      photo_comment_user_cnt: {
        type: DataTypes.DOUBLE,
      },
      photo_like_cnt: {
        type: DataTypes.DOUBLE,
      },
      photo_like_user_cnt: {
        type: DataTypes.DOUBLE,
      },
      video_written_cnt: {
        type: DataTypes.DOUBLE,
      },
      video_written_user_cnt: {
        type: DataTypes.DOUBLE,
      },
      video_comment_cnt: {
        type: DataTypes.DOUBLE,
      },
      video_comment_user_cnt: {
        type: DataTypes.DOUBLE,
      },
      video_like_cnt: {
        type: DataTypes.DOUBLE,
      },
      video_like_user_cnt: {
        type: DataTypes.DOUBLE,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  daily_report.associate = db => {};
  return daily_report;
};
