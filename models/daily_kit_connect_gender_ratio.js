module.exports = (sequelize, DataTypes) => {
  const daily_kit_connect_gender_ratio = sequelize.define(
    'daily_kit_connect_gender_ratio',
    {
      idx: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      age: {
        type: DataTypes.ENUM,
        values: ['unknown', '18-24', '25-34', '35-44', '45-54', '55-65', '(not set)'],
      },
      gender: {
        type: DataTypes.ENUM,
        values: ['unknown', 'female', 'male', '(not set)'],
      },
      value: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      date_timestamp: {
        type: DataTypes.DATE,
        allowNull: DataTypes.NOW,
      },
    },
    {
      charset: 'utf8',
      tableName: 'daily_kit_connect',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  daily_kit_connect_gender_ratio.associate = db => {};
  return daily_kit_connect_gender_ratio;
};
