module.exports = (sequelize, DataTypes) => {
  const albumInfo = sequelize.define(
    'kitinside_user_album_info',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
      },
      company_id: {
        type: DataTypes.INTEGER,
      },
      app_id: {
        type: DataTypes.STRING(50),
      },
      app_grp: {
        type: DataTypes.STRING(50),
      },
      artist: {
        type: DataTypes.STRING(50),
      },
      albumName: {
        type: DataTypes.STRING(100),
      },
      coverImageNum: {
        type: DataTypes.INTEGER,
      },
      package_nm_short: {
        type: DataTypes.STRING(200),
      },
      releaseDate: {
        type: DataTypes.DATE,
      },
      email: {
        type: DataTypes.STRING(50),
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
      timestamp: false,
    },
  );
  albumInfo.associate = db => {};
  return albumInfo;
};
