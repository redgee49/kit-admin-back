module.exports = (sequelize, DataTypes) => {
  const company = sequelize.define(
    'company',
    {
      company: {
        type: DataTypes.STRING(45),
        allowNull: false,
      },
      team: {
        type: DataTypes.STRING(45),
        allowNull: false,
      },
      artist: {
        type: DataTypes.STRING(45),
        allowNull: false,
      },
      date: {
        type: DataTypes.DATE,
        allowNull: DataTypes.NOW,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  company.associate = db => {};
  return company;
};
