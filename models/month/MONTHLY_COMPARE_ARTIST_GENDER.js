module.exports = (sequelize, DataTypes) => {
  const MONTHLY_COMPARE_ARTIST_GENDER = sequelize.define(
    'MONTHLY_COMPARE_ARTIST_GENDER',
    {
      app_id: {
        type: DataTypes.STRING(25),
      },
      label: {
        type: DataTypes.ENUM,
        values: ['Female', 'Male'],
      },
      value: {
        type: DataTypes.INTEGER,
      },
      date_timestamp: {
        type: DataTypes.DATE,
        allowNull: DataTypes.NOW,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  MONTHLY_COMPARE_ARTIST_GENDER.associate = db => {};
  return MONTHLY_COMPARE_ARTIST_GENDER;
};
