module.exports = (sequelize, DataTypes) => {
  const MONTHLY_APP_ARTIST_EVENT = sequelize.define(
    'MONTHLY_APP_ARTIST_EVENT',
    {
      app_id: {
        type: DataTypes.STRING(25),
      },
      event: {
        type: DataTypes.STRING(10),
        allowNull: false,
      },
      value: {
        type: DataTypes.INTEGER,
      },
      date_timestamp: {
        type: DataTypes.DATE,
        allowNull: DataTypes.NOW,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  MONTHLY_APP_ARTIST_EVENT.associate = db => {};
  return MONTHLY_APP_ARTIST_EVENT;
};
