module.exports = (sequelize, DataTypes) => {
  const MONTHLY_COUNTRY_USER = sequelize.define(
    'MONTHLY_COUNTRY_USER',
    {
      app_id: {
        type: DataTypes.STRING(25),
      },
      country: {
        type: DataTypes.STRING(15),
      },
      value: {
        type: DataTypes.INTEGER,
      },
      date_timestamp: {
        type: DataTypes.DATE,
        allowNull: DataTypes.NOW,
      },
      isNew: {
        type: DataTypes.INTEGER,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  MONTHLY_COUNTRY_USER.associate = db => {};
  return MONTHLY_COUNTRY_USER;
};
