module.exports = (sequelize, DataTypes) => {
  const MONTHLY_COUNTRY_TOP_PHOTO = sequelize.define(
    'MONTHLY_COUNTRY_TOP_PHOTO',
    {
      app_id: {
        type: DataTypes.STRING(25),
      },
      country: {
        type: DataTypes.STRING(10),
      },
      value: {
        type: DataTypes.INTEGER,
      },
      date_timestamp: {
        type: DataTypes.DATE,
        allowNull: DataTypes.NOW,
      },
      img_path: {
        type: DataTypes.STRING(100),
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  MONTHLY_COUNTRY_TOP_PHOTO.associate = db => {};
  return MONTHLY_COUNTRY_TOP_PHOTO;
};
