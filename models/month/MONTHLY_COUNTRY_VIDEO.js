module.exports = (sequelize, DataTypes) => {
  const MONTHLY_COUNTRY_VIDEO = sequelize.define(
    'MONTHLY_COUNTRY_VIDEO',
    {
      app_id: {
        type: DataTypes.STRING(25),
      },
      video_id: {
        type: DataTypes.STRING(10),
      },
      country: {
        type: DataTypes.STRING(15),
      },
      value: {
        type: DataTypes.INTEGER,
      },
      date_timestamp: {
        type: DataTypes.DATE,
        allowNull: DataTypes.NOW,
      },
      videoNum: {
        type: DataTypes.STRING(5),
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  MONTHLY_COUNTRY_VIDEO.associate = db => {};
  return MONTHLY_COUNTRY_VIDEO;
};
