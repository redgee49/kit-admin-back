module.exports = (sequelize, DataTypes) => {
  const MONTHLY_COMPARE_ARTIST_AGE = sequelize.define(
    'MONTHLY_COMPARE_ARTIST_AGE',
    {
      app_id: {
        type: DataTypes.STRING(25),
      },
      label: {
        type: DataTypes.STRING(10),
      },
      value: {
        type: DataTypes.INTEGER,
      },
      date_timestamp: {
        type: DataTypes.DATE,
        allowNull: DataTypes.NOW,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  MONTHLY_COMPARE_ARTIST_AGE.associate = db => {};
  return MONTHLY_COMPARE_ARTIST_AGE;
};
