module.exports = (sequelize, DataTypes) => {
  const MONTHLY_COUNTRY_TRACK = sequelize.define(
    'MONTHLY_COUNTRY_TRACK',
    {
      app_id: {
        type: DataTypes.STRING(25),
      },
      country: {
        type: DataTypes.STRING(15),
      },
      value: {
        type: DataTypes.INTEGER,
      },
      startDate: {
        type: DataTypes.DATE,
        allowNull: DataTypes.NOW,
      },
      endDate: {
        type: DataTypes.DATE,
        allowNull: DataTypes.NOW,
      },
      trackNum: {
        type: DataTypes.STRING(10),
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  MONTHLY_COUNTRY_TRACK.associate = db => {};
  return MONTHLY_COUNTRY_TRACK;
};
