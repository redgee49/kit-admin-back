module.exports = (sequelize, DataTypes) => {
  const MONTHLY_ALBUM = sequelize.define(
    'MONTHLY_ALBUM',
    {
      app_id: {
        type: DataTypes.STRING(25),
      },
      value: {
        type: DataTypes.INTEGER,
      },
      date_timestamp: {
        type: DataTypes.DATE,
        allowNull: DataTypes.NOW,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  MONTHLY_ALBUM.associate = db => {};
  return MONTHLY_ALBUM;
};
