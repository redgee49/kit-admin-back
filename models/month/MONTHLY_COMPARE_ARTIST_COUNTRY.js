module.exports = (sequelize, DataTypes) => {
  const MONTHLY_COMPARE_ARTIST_COUNTRY = sequelize.define(
    'MONTHLY_COMPARE_ARTIST_COUNTRY',
    {
      app_id: {
        type: DataTypes.STRING(25),
      },
      country: {
        type: DataTypes.STRING(15),
      },
      value: {
        type: DataTypes.INTEGER,
      },
      date_timestamp: {
        type: DataTypes.DATE,
        allowNull: DataTypes.NOW,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  MONTHLY_COMPARE_ARTIST_COUNTRY.associate = db => {};
  return MONTHLY_COMPARE_ARTIST_COUNTRY;
};
