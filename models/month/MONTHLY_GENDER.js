module.exports = (sequelize, DataTypes) => {
  const MONTHLY_GENDER = sequelize.define(
    'MONTHLY_GENDER',
    {
      app_id: {
        type: DataTypes.STRING(25),
      },
      value: {
        type: DataTypes.INTEGER,
      },
      date_timestamp: {
        type: DataTypes.DATE,
        allowNull: DataTypes.NOW,
      },
      label: {
        type: DataTypes.ENUM,
        values: ['Female', 'Male'],
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  MONTHLY_GENDER.associate = db => {};
  return MONTHLY_GENDER;
};
