module.exports = (sequelize, DataTypes) => {
  const MONTHLY_APP_USAGE_BY_ALBUM = sequelize.define(
    'MONTHLY_APP_USAGE_BY_ALBUM',
    {
      app_id: {
        type: DataTypes.STRING(25),
      },
      comparable_name: {
        type: DataTypes.STRING(60),
      },
      comparable_value: {
        type: DataTypes.INTEGER,
      },
      value: {
        type: DataTypes.INTEGER,
      },
      date_timestamp: {
        type: DataTypes.DATE,
        allowNull: DataTypes.NOW,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  MONTHLY_APP_USAGE_BY_ALBUM.associate = db => {};
  return MONTHLY_APP_USAGE_BY_ALBUM;
};
