module.exports = (sequelize, DataTypes) => {
  const MONTHLY_COUNTRY_EVENT = sequelize.define(
    'MONTHLY_COUNTRY_EVENT',
    {
      app_id: {
        type: DataTypes.STRING(25),
      },
      country: {
        type: DataTypes.STRING(15),
      },
      event: {
        type: DataTypes.ENUM,
        values: ['music', 'video', 'photo'],
      },
      value: {
        type: DataTypes.INTEGER,
      },
      date_timestamp: {
        type: DataTypes.DATE,
        allowNull: DataTypes.NOW,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  MONTHLY_COUNTRY_EVENT.associate = db => {};
  return MONTHLY_COUNTRY_EVENT;
};
