module.exports = (sequelize, DataTypes) => {
  const MONTHLY_APP_AGE = sequelize.define(
    'MONTHLY_APP_AGE',
    {
      app_id: {
        type: DataTypes.STRING(25),
      },
      label: {
        type: DataTypes.STRING(10),
        allowNull: false,
      },
      value: {
        type: DataTypes.INTEGER,
      },
      date_timestamp: {
        type: DataTypes.DATE,
        allowNull: DataTypes.NOW,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  MONTHLY_APP_AGE.associate = db => {};
  return MONTHLY_APP_AGE;
};
