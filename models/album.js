module.exports = (sequelize, DataTypes) => {
  const album = sequelize.define(
    'album',
    {
      app_id: {
        type: DataTypes.STRING(45),
        allowNull: false,
      },
      company_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      date: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  album.associate = db => {};
  return album;
};
