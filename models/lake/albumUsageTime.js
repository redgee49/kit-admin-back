module.exports = (sequelize, DataTypes) => {
  const albumUsageTime = sequelize.define(
    'LAKE_albumUsageTime',
    {
      partition_date: {
        type: DataTypes.DATE,
      },
      app_id: {
        type: DataTypes.STRING(50),
      },
      usage_time: {
        type: DataTypes.STRING(2),
      },
      value: {
        type: DataTypes.INTEGER,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  albumUsageTime.associate = db => {};
  return albumUsageTime;
};
