module.exports = (sequelize, DataTypes) => {
  const endMusicPlay = sequelize.define(
    'LAKE_endMusicPlay',
    {
      partition_date: {
        type: DataTypes.DATE,
      },
      app_id: {
        type: DataTypes.STRING(50),
      },
      title_code: {
        type: DataTypes.STRING(30),
      },
      play_cnt: {
        type: DataTypes.INTEGER,
      },
      song_title: {
        type: DataTypes.STRING(20),
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  endMusicPlay.associate = db => {};
  return endMusicPlay;
};
