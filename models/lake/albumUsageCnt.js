module.exports = (sequelize, DataTypes) => {
  const albumUsageCnt = sequelize.define(
    'LAKE_albumUsageCnt',
    {
      partition_date: {
        type: DataTypes.DATE,
      },
      app_id: {
        type: DataTypes.STRING(50),
      },
      usage_cnt: {
        type: DataTypes.INTEGER,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  albumUsageCnt.associate = db => {};
  return albumUsageCnt;
};
