module.exports = (sequelize, DataTypes) => {
  const albumDeviceModel = sequelize.define(
    'LAKE_albumDeviceModel',
    {
      inquiry_date: {
        type: DataTypes.DATE,
      },
      app_id: {
        type: DataTypes.STRING(50),
      },
      device_mobile_brand_name: {
        type: DataTypes.STRING(2),
      },
      value: {
        type: DataTypes.INTEGER,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  albumDeviceModel.associate = db => {};
  return albumDeviceModel;
};
