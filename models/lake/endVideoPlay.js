module.exports = (sequelize, DataTypes) => {
  const endVideoPlay = sequelize.define(
    'LAKE_endVideoPlay',
    {
      partition_date: {
        type: DataTypes.DATE,
      },
      app_id: {
        type: DataTypes.STRING(50),
      },
      video_id: {
        type: DataTypes.STRING(30),
      },
      play_cnt: {
        type: DataTypes.INTEGER,
      },
      video_title: {
        type: DataTypes.STRING(100),
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    },
  );
  endVideoPlay.associate = db => {};
  return endVideoPlay;
};
