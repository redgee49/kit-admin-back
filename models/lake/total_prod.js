module.exports = (sequelize, DataTypes) => {
  const total_prod = sequelize.define(
    'LAKE_total_prod',
    {
      app_id: {
        type: DataTypes.STRING(50),
      },
      total_prod: {
        type: DataTypes.BIGINT,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    },
  );
  total_prod.associate = db => {};
  return total_prod;
};
