module.exports = (sequelize, DataTypes) => {
  const albumGeography = sequelize.define(
    'LAKE_albumGeography',
    {
      start_date: {
        type: DataTypes.DATE,
      },
      end_date: {
        type: DataTypes.DATE,
      },
      app_id: {
        type: DataTypes.STRING(50),
      },
      continent: {
        type: DataTypes.STRING(50),
      },
      country: {
        type: DataTypes.STRING(50),
      },
      region: {
        type: DataTypes.STRING(50),
      },
      city: {
        type: DataTypes.STRING(50),
      },
      total_user_cnt: {
        type: DataTypes.INTEGER,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  albumGeography.associate = db => {};
  return albumGeography;
};
