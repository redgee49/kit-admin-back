module.exports = (sequelize, DataTypes) => {
  const albumDeviceLanguage = sequelize.define(
    'LAKE_albumDeviceLanguage',
    {
      inquiry_date: {
        type: DataTypes.DATE,
      },
      app_id: {
        type: DataTypes.STRING(50),
      },
      device_language: {
        type: DataTypes.STRING(2),
      },
      value: {
        type: DataTypes.INTEGER,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  albumDeviceLanguage.associate = db => {};
  return albumDeviceLanguage;
};
