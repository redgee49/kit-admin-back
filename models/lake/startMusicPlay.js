module.exports = (sequelize, DataTypes) => {
  const startMusicPlay = sequelize.define(
    'LAKE_startMusicPlay',
    {
      partition_date: {
        type: DataTypes.DATE,
      },
      app_id: {
        type: DataTypes.STRING(50),
      },
      play_cnt: {
        type: DataTypes.INTEGER,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  startMusicPlay.associate = db => {};
  return startMusicPlay;
};
