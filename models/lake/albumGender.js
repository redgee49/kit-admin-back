module.exports = (sequelize, DataTypes) => {
  const albumGender = sequelize.define(
    'LAKE_albumGender',
    {
      inquiry_date: {
        type: DataTypes.DATE,
      },
      app_id: {
        type: DataTypes.STRING(50),
      },
      dimension: {
        type: DataTypes.STRING(20),
      },
      value: {
        type: DataTypes.INTEGER,
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  albumGender.associate = db => {};
  return albumGender;
};
