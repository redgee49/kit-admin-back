module.exports = (sequelize, DataTypes) => {
  const LAKE_coordinates = sequelize.define(
    'LAKE_coordinates',
    {
      category: {
        type: DataTypes.STRING(10),
      },
      name: {
        type: DataTypes.STRING(50),
      },
      latitude: {
        type: DataTypes.STRING(50),
      },
      longtitude: {
        type: DataTypes.STRING(50),
      },
    },
    {
      charset: 'utf8',
      freezeTableName: true,
      collate: 'utf8_general_ci',
    }
  );
  LAKE_coordinates.associate = db => {};
  return LAKE_coordinates;
};
